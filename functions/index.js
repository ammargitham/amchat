'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

/**
 * Triggers when a user writes a new chat and sends a notification to the toUser.
 *
 * Users add a chat to `/users/{toUserId}/conversations/{fromUserId}/`.
 * Users save their device notification tokens to `/users/{userId}/notificationTokens/{notificationToken}`.
 */
exports.sendChatNotification = functions.database.ref('/users/{toUserId}/conversations/{fromUserId}/{pushId}').onWrite(event => {

    // If delete chat event then exit
    if (!event.data.val()) {
        return;
    }
    const toUserId = event.params.toUserId;
    const fromUserId = event.params.fromUserId;
    const pushId = event.params.pushId
    //admin.database.enableLogging(true);
    // Get the list of device notification tokens.
    const getToDeviceTokensPromise = admin.database()
        .ref('/users/' + toUserId + '/notificationTokens')
        .orderByKey()
        .once('value');
    // Get the list of device notification tokens.
    const getFromDeviceTokensPromise = admin.database()
         .ref('/users/' + fromUserId + '/notificationTokens')
         .orderByKey()
         .once('value');
    // Get the from profile.
    //const getFromUserPromise = admin.auth().getUser(fromUserId);
    const isNew = !event.data.previous.val();
    const actualDeviceTokenPromise = isNew ? getToDeviceTokensPromise : getFromDeviceTokensPromise;
    return Promise.all([actualDeviceTokenPromise/*, getFromUserPromise*/]).then(results => {
        const tokensSnapshot = results[0];
        //const fromUser = results[1];
        var status = 0;
        if (isNew) {/*New message*/}
        else {
            // status updated
            status = event.data.val().status;
            if(status == 11) {
                // if read status, then delete this message
                var messageRef = admin.database().ref('/users/' + toUserId + '/conversations/' + fromUserId + '/' + pushId);
                messageRef.remove().then(function() {
                    console.log("Remove succeeded.")
                });
            }
        }
        // Listing all toUser tokens.
        var tokens = [];
        var tokenKeys = [];
        tokensSnapshot.forEach(function (childSnapshot) {
            tokenKeys.push(childSnapshot.key);
            tokens.push(childSnapshot.val());
        });
        // Check if there are any device tokens.
        if (tokens.length == 0) {
            return console.log('There are no notification tokens to send to.');
        }
        const original = event.data.val();
        //console.log('status', status);
        var data = {};
        data.fromUid = fromUserId;
        data.messageKey = pushId;
        data.message = original.string;
        data.messageStatus = status + '';
        // Notification details.
        const payload = {
            data: data
        };
        // Send notifications to all tokens.
        return admin.messaging().sendToDevice(tokens, payload).then(response => {
            // For each message check if there was an error.
            const tokensToRemove = [];
            response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                    console.error('Failure sending notification to', tokens[index], error);
                    // Cleanup the tokens who are not registered anymore.
                    if (error.code === 'messaging/invalid-registration-token' ||
                        error.code === 'messaging/registration-token-not-registered') {
                        tokensToRemove.push(tokensSnapshot.ref.child(tokenKeys[index]).remove());
                    }
                }
            });
            return Promise.all(tokensToRemove);
        });
    });
});