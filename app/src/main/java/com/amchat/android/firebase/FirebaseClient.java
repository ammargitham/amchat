package com.amchat.android.firebase;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.amchat.android.conversations.SearchResult;
import com.amchat.android.models.Chat;
import com.amchat.android.models.Conversation;
import com.amchat.android.models.User;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Maybe;
import io.reactivex.MaybeEmitter;
import io.reactivex.MaybeOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

/**
 * Created by Ammar Githam on 03/03/2017.
 */
public class FirebaseClient {

    private static final String USER_NAME = "name";
    private static final String TAG       = "FirebaseClient";
    private final FirebaseDatabase database;

    public FirebaseClient(@NonNull FirebaseDatabase database) {
        this.database = database;
    }

    public List<Conversation> getConversations() {
        List<Conversation> conversations = new ArrayList<>();
        return conversations;
    }

    public Single<User> addUser(final User user) {
        return Single.create(new SingleOnSubscribe<User>() {
            @Override
            public void subscribe(final SingleEmitter<User> e) throws Exception {
                DatabaseReference ref = database.getReference("usernames");
                ref.child(user.getName()).setValue(user.getServerKey(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError error, DatabaseReference ref) {
                        if (error != null) {
                            Log.e(TAG, "Error in adding user to Firebase database", error.toException());
                            e.onError(error.toException());
                            return;
                        }
                        ref = database.getReference("users");
                        ref.child(user.getServerKey())
                           .child(USER_NAME)
                           .setValue(user.getName(), new DatabaseReference.CompletionListener() {
                               @Override
                               public void onComplete(DatabaseError error1, DatabaseReference ref1) {
                                   if (error1 != null) {
                                       Log.e(TAG, "Error in adding user to Firebase database", error1.toException());
                                       e.onError(error1.toException());
                                       return;
                                   }
                                   e.onSuccess(user);
                               }
                           });
                    }
                });
            }
        });
    }

    public Single<Boolean> checkUsernameExists(final String username) {
        return Single.create(new SingleOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final SingleEmitter<Boolean> e) throws Exception {
                DatabaseReference ref = database.getReference("usernames");
                ref.orderByKey()
                   .equalTo(username)
                   .addListenerForSingleValueEvent(new ValueEventListener() {
                       @Override
                       public void onDataChange(DataSnapshot dataSnapshot) {
                           e.onSuccess(dataSnapshot.exists());
                       }

                       @Override
                       public void onCancelled(DatabaseError error) {
                           e.onError(error.toException());
                       }
                   });
            }
        });
    }

    public Maybe<String> getUsername(final String uid) {
        return Maybe.create(new MaybeOnSubscribe<String>() {
            @Override
            public void subscribe(final MaybeEmitter<String> e) throws Exception {
                database.getReference("usernames")
                        .orderByValue()
                        .equalTo(uid)
                        .addListenerForSingleValueEvent(
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            //Log.d(TAG, "onDataChange: dataSnapshot: " + dataSnapshot);
                                            Iterable<DataSnapshot> dataSnapshots = dataSnapshot.getChildren();
                                            for (DataSnapshot childSnapshot : dataSnapshots) {
                                                //Log.d(TAG, "onDataChange: childSnapshot: " + childSnapshot);
                                                if (childSnapshot == null) {
                                                    continue;
                                                }
                                                e.onSuccess(childSnapshot.getKey());
                                                return;
                                            }
                                        }
                                        e.onComplete();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        e.onError(databaseError.toException());
                                    }
                                });
            }
        });
    }

    public Maybe<Pair<String, List<SearchResult>>> searchUsernames(final String query) {

        return Maybe.create(new MaybeOnSubscribe<Pair<String, List<SearchResult>>>() {
            @Override
            public void subscribe(final MaybeEmitter<Pair<String, List<SearchResult>>> e) throws Exception {
                if (TextUtils.isEmpty(query)) {
                    // Return empty list
                    e.onSuccess(Pair.create(query, Collections.<SearchResult>emptyList()));
                    return;
                }
                DatabaseReference ref = database.getReference("usernames");
                ref.orderByKey().startAt(query).endAt(query + "\uf8ff").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Log.d(TAG, "onDataChange: dataSnapshot:" + dataSnapshot.toString());
                        List<SearchResult> searchResults = new ArrayList<>();
                        if (dataSnapshot.exists()) {
                            Iterable<DataSnapshot> dataSnapshots = dataSnapshot.getChildren();
                            if (dataSnapshots != null) {
                                for (DataSnapshot childSnapshot : dataSnapshots) {
                                    if (childSnapshot == null) {
                                        continue;
                                    }
                                    //Log.d(TAG, "onDataChange: childSnapshot: " + childSnapshot.toString());
                                    searchResults.add(
                                            new SearchResult().setId(childSnapshot.getValue(String.class))
                                                              .setName(childSnapshot.getKey())
                                                              .setType(SearchResult.TYPE_USER));
                                }
                            }
                        }
                        e.onSuccess(Pair.create(query, searchResults));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }

    public void deleteAccount(final FirebaseUser user) {
        if (user == null) {
            return;
        }
        // Move delete account logic to Functions

        /*// Get username from firebase db
        getUsername(user.getUid())
                .observeOn(Schedulers.io())
                .flatMap(new Function<String, Observable<?>>() {
                    @Override
                    public ObservableSource<?> apply(String s) throws Exception {
                        return null;
                    }
                });
        // Remove username from usernames
        DatabaseReference ref = database.getReference("usernames");
        ref = ref.child(dataSnapshot.getValue().toString());
        if (ref != null) {
            ref.removeValue();
        }
        //Remove user data from firebase database
        ref = database.getReference("users");
        ref = ref.child(user.getUid());
        if (ref != null) {
            ref.removeValue();
        }
        // Remove user from firebase
        Log.d(TAG, "Removing user account");
        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "User deleted from firebase");
                }
                else {
                    Log.e(TAG, "Error in deleting user", task.getException());
                }
            }
        });
    }

}

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Log.e(TAG, "onCancelled: error getting username", databaseError.toException());
    }
    });
}*/
    }

    public Single<String> sendChat(final Chat chat, final User fromUser, final User toUser) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(final SingleEmitter<String> e) throws Exception {
                DatabaseReference ref = database.getReference(
                        "users/" + toUser.getServerKey() + "/conversations/" + fromUser.getServerKey() + "/");
                ref = ref.push();
                final String chatKey = ref.getKey();
                Map<String, Object> value = new HashMap<>();
                value.put("timestamp", ServerValue.TIMESTAMP);
                value.put("string", chat.getChatString());
                ref.setValue(value, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            e.onError(databaseError.toException());
                            return;
                        }
                        e.onSuccess(chatKey);
                    }
                });
            }
        });
    }

    public Single<String> addDeviceTokenToUser(final User user, final String token) {
        //Check if token already exists for this user
        return getTokenKeyForUser(user, token)
                .switchIfEmpty(
                        // Token does not exist.
                        // So creating a new entry for this token.
                        Maybe.fromSingle(Single.create(new SingleOnSubscribe<String>() {
                            @Override
                            public void subscribe(final SingleEmitter<String> e) throws Exception {
                                DatabaseReference ref = database.getReference(
                                        "users/" + user.getServerKey() + "/notificationTokens/");
                                ref = ref.push();
                                final String tokenKey = ref.getKey();
                                ref.setValue(token, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError,
                                            DatabaseReference databaseReference) {
                                        if (databaseError != null) {
                                            e.onError(databaseError.toException());
                                            return;
                                        }
                                        e.onSuccess(tokenKey);
                                    }
                                });
                            }
                        })))
                .toSingle();
    }

    private Maybe<String> getTokenKeyForUser(final User user, final String token) {
        return Maybe.create(new MaybeOnSubscribe<String>() {
            @Override
            public void subscribe(final MaybeEmitter<String> e) throws Exception {
                DatabaseReference ref = database.getReference("users/" + user.getServerKey() + "/notificationTokens/");
                ref.orderByValue().equalTo(token).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            e.onSuccess(dataSnapshot.getKey());
                            return;
                        }
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }

    public Completable setChatStatus(final String fromUid, final String toUid, final List<String> messageKeys, final int status) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(final CompletableEmitter e) throws Exception {
                DatabaseReference ref = database.getReference("users/" + toUid + "/conversations/" + fromUid);
                Map<String, Object> statusMap = new HashMap<>();
                for (String messageKey : messageKeys) {
                    statusMap.put(messageKey + "/status", status);
                }
                ref.updateChildren(statusMap, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            e.onError(databaseError.toException());
                            return;
                        }
                        e.onComplete();
                    }
                });
            }
        });
    }
}
