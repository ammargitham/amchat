package com.amchat.android.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.amchat.android.R;
import com.amchat.android.services.AmchatFirebaseMessagingService;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ammar Githam on 08/03/2017.
 */
public class Utilities {

    public static void dismissUnreadNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(AmchatFirebaseMessagingService.MESSAGE_NOTIFICATION_ID);
    }

    static public class ValidationResult {

        private boolean isValid;
        private int     reasonResource;

        public boolean isValid() {
            return isValid;
        }

        void setValid(boolean valid) {
            isValid = valid;
        }

        public int getReasonResource() {
            return reasonResource;
        }

        void setReasonSource(int reasonResource) {
            this.reasonResource = reasonResource;
        }
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static ValidationResult validatePassword(String password) {
        ValidationResult validationResult = new ValidationResult();
        boolean isValid = !TextUtils.isEmpty(password);
        int reasonResource = R.string.error_field_required;
        if (isValid) {
            isValid = password.length() > 4;
            reasonResource = R.string.error_invalid_password;
        }
        validationResult.setValid(isValid);
        if (!isValid) {
            validationResult.setReasonSource(reasonResource);
        }
        return validationResult;
    }

    public static ValidationResult validateEmail(String email) {
        ValidationResult validationResult = new ValidationResult();
        boolean isValid = !TextUtils.isEmpty(email);
        int reasonResource = R.string.error_field_required;
        if (isValid) {
            isValid = email.contains("@");
            reasonResource = R.string.error_invalid_email;
        }
        validationResult.setValid(isValid);
        if (!isValid) {
            validationResult.setReasonSource(reasonResource);
        }
        return validationResult;
    }

    public static ValidationResult validateUsername(String username) {
        ValidationResult validationResult = new ValidationResult();
        username = username.trim();
        boolean isValid = !TextUtils.isEmpty(username);
        int reasonResource = R.string.error_field_required;
        if (isValid) {
            isValid = username.length() >= 5;
            reasonResource = R.string.error_username_short;
        }
        if (isValid) {
            isValid = !username.contains(" ");
            reasonResource = R.string.error_username_contains_spaces;
        }
        validationResult.setValid(isValid);
        if (!isValid) {
            validationResult.setReasonSource(reasonResource);
        }
        return validationResult;
    }

    private static Calendar getDayStartCalendarForDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar;
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillis = getDayStartCalendarForDate(date2).getTimeInMillis() - getDayStartCalendarForDate(date1).getTimeInMillis();
        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }
}
