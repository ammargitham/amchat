package com.amchat.android.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.amchat.android.AmchatApplication;
import com.amchat.android.R;
import com.amchat.android.conversations.ChatActivity;
import com.amchat.android.conversations.ConversationsActivity;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.firebase.FirebaseClient;
import com.amchat.android.models.Chat;
import com.amchat.android.models.ChatEntity;
import com.amchat.android.models.Conversation;
import com.amchat.android.models.User;
import com.amchat.android.models.UserEntity;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;

public class AmchatFirebaseMessagingService extends FirebaseMessagingService {

    private static final String              TAG                                      = "FiBaMessagingService";
    public static final  int                 MESSAGE_NOTIFICATION_ID                  = 0;
    private static final int                 NOTIFICATION_PENDING_INTENT_REQUEST_CODE = 10;
    private              CompositeDisposable mDisposables                             = new CompositeDisposable();
    private User           mCurrentFromUser;
    private Chat           mCurrentChat;
    private DatabaseClient mDatabaseClient;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        //Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        final Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Log.d(TAG, "Message data payload: " + data);
            final String fromUid = data.get("fromUid");
            final String message = data.get("message");
            final String messageKey = data.get("messageKey");
            String messageStatusString = data.get("messageStatus");
            int messageStatus = 0;
            if (messageStatusString != null) {
                messageStatus = Integer.parseInt(messageStatusString);
            }
            final int finalMessageStatus = messageStatus;
            // Save to db
            mDatabaseClient = new DatabaseClient(((AmchatApplication) getApplication()).getData());
            final FirebaseClient firebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
            // Get from user object
            mDisposables.add(
                    mDatabaseClient
                            .getUser(fromUid)
                            // If user not saved in local db, get info from firebase, and save to local db
                            .switchIfEmpty(Maybe.fromSingle(
                                    Single.defer(new Callable<Single<User>>() {
                                        @Override
                                        public Single<User> call() throws Exception {
                                            Log.d(TAG, "apply: User not found in local db, getting info from firebase...");
                                            String username = firebaseClient.getUsername(fromUid).blockingGet();
                                            if (username == null) {
                                                throw new IllegalArgumentException("Username for uid " + fromUid + " not found in firebase");
                                            }
                                            Log.d(TAG, "apply: Got username: " + username);
                                            UserEntity user = new UserEntity();
                                            user.setCurrentUser(false);
                                            user.setServerKey(fromUid);
                                            user.setName(username);
                                            return mDatabaseClient.saveUser(user);
                                        }
                                    })))
                            .toSingle()
                            .doOnError(new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) throws Exception {
                                    Log.e(TAG, "accept: Error retrieving username from firebase for uid: ", throwable);
                                    // Maybe log this error to Firebase for security check
                                }
                            })
                            // Mark this chat status delivered in firebase if new message
                            .map(new Function<User, User>() {
                                @Override
                                public User apply(User user) throws Exception {
                                    if (finalMessageStatus == 0) {
                                        // New message
                                        String toUid = mDatabaseClient.getCurrentUser().blockingGet().getServerKey();
                                        firebaseClient.setChatStatus(fromUid, toUid, Collections.singletonList(messageKey),
                                                Chat.STATUS_RECEIVED_UNREAD)
                                                      .blockingAwait();
                                    }
                                    return user;
                                }
                            })
                            .flatMap(new Function<User, Single<Chat>>() {
                                @Override
                                public Single<Chat> apply(User user) throws Exception {
                                    //Log.d(TAG, "apply: From user: " + user);
                                    mCurrentFromUser = user;
                                    switch (finalMessageStatus) {
                                        default:
                                            Log.d(TAG, "apply: Unknown messageStatus: " + finalMessageStatus + " Assuming it as new message...");
                                        case 0:
                                            // New message
                                            // Save the chat with the fromUser id
                                            long userId = user.getId();
                                            Conversation conversation = mDatabaseClient.getConversationForUser(userId).blockingGet();
                                            if (conversation == null) {
                                                conversation = mDatabaseClient.newConversation(userId, user.getServerKey()).blockingGet();
                                            }
                                            ChatEntity chat = new ChatEntity();
                                            chat.setServerKey(messageKey);
                                            chat.setChatString(message);
                                            chat.setTimestamp(new Date().getTime());
                                            chat.setUserId(userId);
                                            chat.setConversationId(conversation.getId());
                                            chat.setStatus(Chat.STATUS_RECEIVED_UNREAD);
                                            //Log.d(TAG, "apply: Saving chat to local db...");
                                            return mDatabaseClient.saveChat(chat);
                                        case Chat.STATUS_RECEIVED_UNREAD:
                                            // Update chat with delivered status
                                            return mDatabaseClient.updateChatStatus(messageKey, Chat.STATUS_SENT_DELIVERED);
                                        case Chat.STATUS_RECEIVED_READ:
                                            // Update chat with read status
                                            return mDatabaseClient.updateChatStatus(messageKey, Chat.STATUS_SENT_READ);
                                    }
                                }
                            })
                            .map(new Function<Chat, Chat>() {
                                @Override
                                public Chat apply(Chat chat) throws Exception {
                                    if (finalMessageStatus == 0) {
                                        // Update the lastChatId for the conversation
                                        mCurrentChat = chat;
                                        mDatabaseClient.setLastChat(chat.getConversationId(), chat.getId());
                                    }
                                    return chat;
                                }
                            })
                            .subscribeWith(new DisposableSingleObserver<Chat>() {
                                @Override
                                public void onSuccess(Chat chat) {
                                    //Log.d(TAG, "onSuccess: chat: " + chat);
                                    // Post the message event
                                    EventBus.getDefault().post(new MessageEvent(fromUid, finalMessageStatus));
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.e(TAG, "onError: ", e);
                                }
                            }));
        }
    }

    private void sendNotification(List<Chat> chats) {

        Set<Long> conversationIds = new HashSet<>();
        for (Chat chat : chats) {
            conversationIds.add(chat.getConversationId());
        }

        String title = conversationIds.size() == 1 ? mCurrentFromUser.getName() : "Amchat";
        String contentText =
                // If only from 1 conversation/user
                conversationIds.size() == 1 ?
                        // Show the chat if only 1 chat, else show the number of unread
                        chats.size() == 1 ? mCurrentChat.getChatString() : chats.size() + " new chats"
                        // If unread from multiple conversations, show number of unread from number of conversations
                        : chats.size() + " new chats from " + conversationIds.size() + " conversations";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_menu_camera)
                .setContentTitle(title)
                .setContentText(contentText)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{0, 500});

        // Setting the expanded view
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(title);
        // If only 1 unread chat, then show app name as summary text, else the contentText
        inboxStyle.setSummaryText(chats.size() == 1 ? "Amchat" : contentText);
        for (Chat chat : chats) {
            // Adding line as: <conversation_name>: <chat_string>
            // Currently username is the conversation name
            // If only one conversation don't append the conversation_name
            inboxStyle.addLine(
                    (conversationIds.size() != 1 ? mDatabaseClient.getUser(chat.getUserId()).blockingGet().getName() + ": " : "")
                            + chat.getChatString());
        }
        notificationBuilder.setStyle(inboxStyle);

        Intent intent;
        if (conversationIds.size() == 1) {
            // Set pending intent as ChatActivity with user id extra
            intent = new Intent(this, ChatActivity.class);
            intent.putExtra(ChatActivity.USER_UID, mCurrentFromUser.getServerKey());
            intent.putExtra(ChatActivity.USER_NAME, mCurrentFromUser.getName());
            //intent.putExtra(ChatActivity.CONVERSATION_ID, mCurrentChat.getConversationId());
        }
        else {
            // Set pending intent as ConversationActivity
            intent = new Intent(this, ConversationsActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIFICATION_PENDING_INTENT_REQUEST_CODE, intent,
                PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(MESSAGE_NOTIFICATION_ID, notificationBuilder.build());
    }

    @Subscribe(priority = 0)
    public void onMessageEvent(MessageEvent event) {
        if (event.messageStatus == 0) {
            Log.d(TAG, "onMessageEvent: Got event in MessagingService. Showing notification...");
            List<Chat> chats = mDatabaseClient.getUnreadChats().toList();
            sendNotification(chats);
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        mDisposables.clear();
        super.onDestroy();
    }
}
