package com.amchat.android.services;

/**
 * Created by Ammar Githam on 29/03/2017.
 */
public class MessageEvent {

    public final String fromUid;
    public final int    messageStatus;

    MessageEvent(String fromUid, int messageStatus) {
        this.fromUid = fromUid;
        this.messageStatus = messageStatus;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "fromUid='" + fromUid + '\'' +
                ", messageStatus=" + messageStatus +
                '}';
    }
}
