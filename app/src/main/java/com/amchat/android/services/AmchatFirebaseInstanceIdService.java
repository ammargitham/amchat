package com.amchat.android.services;

import android.util.Log;

import com.amchat.android.AmchatApplication;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.firebase.FirebaseClient;
import com.amchat.android.models.AmChatFirebaseInstanceIdEntity;
import com.amchat.android.models.User;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Ammar Githam on 24/03/2017.
 */
public class AmchatFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String              TAG          = "FiBaInstanceIDService";
    private              CompositeDisposable mDisposables = new CompositeDisposable();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the mUser's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        final DatabaseClient databaseClient = new DatabaseClient(((AmchatApplication) getApplication()).getData());
        User user = databaseClient.getCurrentUser().blockingGet();
        if (user == null) {
            // User is not logged in
            return;
        }
        FirebaseClient firebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
        mDisposables.add(
                firebaseClient.addDeviceTokenToUser(user, token)
                              .doOnSuccess(new Consumer<String>() {
                                  @Override
                                  public void accept(String key) throws Exception {
                                      Log.d(TAG, "accept: Token added to firebase db: " + key);
                                  }
                              })
                              .flatMap(new Function<String, Single<AmChatFirebaseInstanceIdEntity>>() {
                                  @Override
                                  public Single<AmChatFirebaseInstanceIdEntity> apply(String s) throws Exception {
                                      return databaseClient.updateInstanceId(token);
                                  }
                              })
                              .subscribeWith(new DisposableSingleObserver<AmChatFirebaseInstanceIdEntity>() {
                                  @Override
                                  public void onSuccess(AmChatFirebaseInstanceIdEntity instanceIdEntity) {
                                      Log.d(TAG, "onSuccess: Added token to local db");
                                  }

                                  @Override
                                  public void onError(Throwable e) {
                                      Log.e(TAG, "onError: Error in saving token to local db", e);
                                  }
                              }));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposables.clear();
    }
}
