package com.amchat.android.models;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;

/**
 * Created by Ammar Githam on 06/03/2017.
 */
@Table(name = "chats")
@Entity
public interface Chat extends Persistable {

    int STATUS_SENT_PENDING    = 0;
    int STATUS_SENT_SENT       = 1;
    int STATUS_SENT_DELIVERED  = 2;
    int STATUS_SENT_READ       = 3;
    int STATUS_RECEIVED_UNREAD = 10;
    int STATUS_RECEIVED_READ   = 11;

    @Key
    @Generated
    long getId();

    @Column(name = "server_key")
    String getServerKey();

    @Column(name = "chat_string")
    String getChatString();

    @Column(name = "user")
    long getUserId();

    @Column(name = "timestamp")
    long getTimestamp();

    @Column(name = "conversation_id")
    long getConversationId();

    @Column(name = "status")
    int getStatus();
}
