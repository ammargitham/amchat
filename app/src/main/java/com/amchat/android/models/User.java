package com.amchat.android.models;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;

/**
 * Created by Ammar Githam on 06/03/2017.
 */
@Table(name = "users")
@Entity
public interface User extends Persistable {

    @Key
    @Generated
    long getId();

    @Column(name = "name")
    String getName();

    @Column(name = "server_key")
    String getServerKey();

    @Column(name = "is_current")
    boolean isCurrentUser();
}
