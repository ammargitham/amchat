package com.amchat.android.models;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;

/**
 * Created by Ammar Githam on 27/03/2017.
 */
@Table(name = "instance_id")
@Entity
public interface AmChatFirebaseInstanceId extends Persistable {

    @Key
    @Generated
    long getId();

    @Column(name = "instance_id")
    String getInstanceId();
}
