package com.amchat.android.models;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;

/**
 * Created by Ammar Githam on 06/03/2017.
 */
@Table(name = "conversations")
@Entity
public interface Conversation extends Persistable {

    @Key
    @Generated
    long getId();

    @Column(name = "server_key")
    String getServerKey();

    @Column(name = "user")
    long getUserId();

    @Column(name = "last_chat_id")
    long getLastChatId();
}
