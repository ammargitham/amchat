package com.amchat.android.login;

import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.transition.Scene;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.amchat.android.AmchatApplication;
import com.amchat.android.R;
import com.amchat.android.conversations.ConversationsActivity;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.firebase.FirebaseClient;
import com.amchat.android.models.User;
import com.amchat.android.models.UserEntity;
import com.amchat.android.utils.RxHelper;
import com.amchat.android.utils.Utilities;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final String              TAG                   = "LoginActivity";
    private static final int                 REQUEST_READ_CONTACTS = 0;
    private static final int                 DEBOUNCE_TIMEOUT      = 500;
    private final        CompositeDisposable mDisposables          = new CompositeDisposable();
    private FirebaseAuth                   mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private AutoCompleteTextView           mEmailView;
    private EditText                       mPasswordView;
    private EditText                       mUsernameView;
    private View                           mProgressView;
    private Button                         mEmailSignInButton;
    private Scene                          mEmailScene;
    private Scene                          mEmailSignInScene;
    private Scene                          mEmailRegisterScene;
    private FirebaseClient                 mFirebaseClient;
    private View                           mUsernameProgressView;
    private DatabaseClient                 mDatabaseClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            Intent conversationActivityIntent = new Intent(LoginActivity.this, ConversationsActivity.class);
            startActivity(conversationActivityIntent);
            finish();
            return;
        }
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                }
                else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        // Create the scenes
        ViewGroup mSceneRoot = (ViewGroup) findViewById(R.id.scene_root);
        mEmailScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_email, this);
        mEmailRegisterScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_email_register, this);
        mEmailSignInScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_email_sign_in, this);
        AmchatApplication mAmchatApplication = (AmchatApplication) getApplication();
        mDatabaseClient = new DatabaseClient(mAmchatApplication.getData());
        mFirebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
        showEmailScene();
    }

    private void showEmailScene() {
        // Set up email scene
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProvidersAndNextScene();
            }
        });
        //View mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        Observable<String> emailObservable = RxHelper.getTextWatcherObservable(mEmailView);
        mDisposables.add(emailObservable
                .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<String, Boolean>() {
                    @Override
                    public Boolean apply(String email) throws Exception {
                        Utilities.ValidationResult result = Utilities.validateEmail(email);
                        mEmailView.setError(!result.isValid() ? getString(result.getReasonResource()) : null);
                        return result.isValid();
                    }
                })
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean enabled) throws Exception {
                        mEmailSignInButton.setEnabled(enabled);
                    }
                }));
    }

    private void getProvidersAndNextScene() {
        if (mAuth == null) {
            return;
        }
        mEmailSignInButton.setEnabled(false);
        final String email = mEmailView.getText().toString().trim();
        mProgressView.setVisibility(View.VISIBLE);
        mAuth.fetchProvidersForEmail(email)
             .addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                 @Override
                 public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                     mProgressView.setVisibility(View.GONE);
                     ProviderQueryResult result = task.getResult();
                     List<String> providers = result.getProviders();
                     if (providers != null && providers.contains("password")) {
                         // Show scene sign in
                         showSceneSignIn(email);
                     }
                     else {
                         // Show scene register
                         showSceneRegister(email);
                     }
                 }
             });
    }

    private void showSceneSignIn(String email) {
        TransitionManager.go(mEmailSignInScene);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setText(email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.requestFocus();
        //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setText(getString(R.string.action_sign_in));
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        mProgressView = findViewById(R.id.login_progress);

        PublishSubject<String> emailViewWatcherSubject = (PublishSubject<String>) RxHelper.getTextWatcherObservable(mEmailView);
        Observable<Boolean> emailObservable =
                emailViewWatcherSubject
                        .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<String, Boolean>() {
                            @Override
                            public Boolean apply(String email) throws Exception {
                                Utilities.ValidationResult result = Utilities.validateEmail(email);
                                mEmailView.setError(!result.isValid() ? getString(result.getReasonResource()) : null);
                                return result.isValid();
                            }
                        });
        Observable<Boolean> passwordObservable =
                RxHelper.getTextWatcherObservable(mPasswordView)
                        .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<String, Boolean>() {
                            @Override
                            public Boolean apply(String password) throws Exception {
                                Utilities.ValidationResult result = Utilities.validatePassword(password);
                                mPasswordView.setError(!result.isValid() ? getString(result.getReasonResource()) : null);
                                return result.isValid();
                            }
                        });
        mDisposables.add(Observable.combineLatest(emailObservable, passwordObservable, new BiFunction<Boolean, Boolean, Boolean>() {
            @Override
            public Boolean apply(Boolean emailValid, Boolean passwordValid) throws Exception {
                return emailValid && passwordValid;
            }
        }).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean enabled) throws Exception {
                mEmailSignInButton.setEnabled(enabled);
            }
        }));
        emailViewWatcherSubject.onNext(email);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        if (mAuth == null) {
            return;
        }
        mEmailSignInButton.setEnabled(false);
        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString().trim();
        String password = mPasswordView.getText().toString();
        // Show a progress spinner, and kick off a background task to
        // perform the user login attempt.
        mProgressView.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email, password)
             .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                 @Override
                 public void onComplete(@NonNull Task<AuthResult> task) {
                     // If sign in fails, display a message to the user. If sign in succeeds
                     // the auth state listener will be notified and logic to handle the
                     // signed in user can be handled in the listener.
                     if (!task.isSuccessful()) {
                         mProgressView.setVisibility(View.GONE);
                         mEmailSignInButton.setEnabled(true);
                         Snackbar.make(mEmailView, R.string.auth_failed, Snackbar.LENGTH_INDEFINITE).show();
                         return;
                     }
                     final String uid = task.getResult().getUser().getUid();
                     mDisposables.add(
                             mFirebaseClient
                                     .getUsername(uid)
                                     .doOnSuccess(new Consumer<String>() {
                                         @Override
                                         public void accept(String username) throws Exception {
                                             Log.d(TAG, "username from firebase: " + username);
                                         }
                                     })
                                     .doOnError(new Consumer<Throwable>() {
                                         @Override
                                         public void accept(Throwable throwable) throws Exception {
                                             Log.e(TAG, "error fetching username from firebase db", throwable);
                                         }
                                     })
                                     .flatMapSingle(new Function<String, Single<User>>() {
                                         @Override
                                         public Single<User> apply(String username) throws Exception {
                                             UserEntity user = new UserEntity();
                                             user.setName(username);
                                             user.setServerKey(uid);
                                             user.setCurrentUser(true);
                                             return mDatabaseClient.saveUser(user);
                                         }
                                     })
                                     .subscribeOn(Schedulers.io())
                                     .observeOn(AndroidSchedulers.mainThread())
                                     .subscribeWith(new DisposableSingleObserver<User>() {
                                         @Override
                                         public void onSuccess(User user) {
                                             mProgressView.setVisibility(View.GONE);
                                             Intent conversationActivityIntent = new Intent(LoginActivity.this, ConversationsActivity.class);
                                             startActivity(conversationActivityIntent);
                                             finish();
                                         }

                                         @Override
                                         public void onError(Throwable e) {
                                             Log.e(TAG, "onError: ", e);
                                             mEmailSignInButton.setEnabled(true);
                                         }
                                     }));
                 }
             });
    }

    private void showSceneRegister(final String email) {
        TransitionManager.go(mEmailRegisterScene);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setText(email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.requestFocus();
        // If we show the keyboard forcefully, we also need to handle its closing.
        //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        mUsernameView = (EditText) findViewById(R.id.username);
        mUsernameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.register || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setText(getString(R.string.action_register));
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });
        mProgressView = findViewById(R.id.login_progress);
        mUsernameProgressView = findViewById(R.id.username_progress);

        PublishSubject<String> emailViewWatcherSubject = (PublishSubject<String>) RxHelper.getTextWatcherObservable(mEmailView);
        Observable<Boolean> emailObservable =
                emailViewWatcherSubject
                        .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<String, Boolean>() {
                            @Override
                            public Boolean apply(String email) throws Exception {
                                Utilities.ValidationResult result = Utilities.validateEmail(email);
                                mEmailView.setError(!result.isValid() ? getString(result.getReasonResource()) : null);
                                return result.isValid();
                            }
                        });
        Observable<Boolean> passwordObservable =
                RxHelper.getTextWatcherObservable(mPasswordView)
                        .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<String, Boolean>() {
                            @Override
                            public Boolean apply(String password) throws Exception {
                                Utilities.ValidationResult result = Utilities.validatePassword(password);
                                mPasswordView.setError(!result.isValid() ? getString(result.getReasonResource()) : null);
                                return result.isValid();
                            }
                        });
        Observable<Boolean> usernameObservable =
                RxHelper.getTextWatcherObservable(mUsernameView)
                        .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<String, Pair<String, Boolean>>() {
                            @Override
                            public Pair<String, Boolean> apply(String username) throws Exception {
                                Utilities.ValidationResult result = Utilities.validateUsername(username);
                                mUsernameView.setError(!result.isValid() ? getString(result.getReasonResource()) : null);
                                return new Pair<>(username, result.isValid());
                            }
                        })
                        .switchMap(new Function<Pair<String, Boolean>, Observable<Boolean>>() {
                            @Override
                            public Observable<Boolean> apply(Pair<String, Boolean> resultPair) throws Exception {
                                Log.d(TAG, "apply: " + resultPair.second);
                                if (resultPair.second) {
                                    mUsernameProgressView.setVisibility(View.VISIBLE);
                                    return mFirebaseClient.checkUsernameExists(resultPair.first).toObservable();
                                }
                                else {
                                    // True means username exists which will disable the register button.
                                    return Observable.just(true);
                                }
                            }
                        })
                        .doOnNext(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean aBoolean) throws Exception {
                                mUsernameProgressView.setVisibility(View.GONE);
                            }
                        })
                        .map(new Function<Boolean, Boolean>() {
                            @Override
                            public Boolean apply(Boolean usernameExists) throws Exception {
                                if (usernameExists) {
                                    mUsernameView.setError(getString(R.string.error_username_exists));
                                }
                                return usernameExists;
                            }
                        });

        mDisposables.add(
                Observable.combineLatest(emailObservable, passwordObservable, usernameObservable,
                        new Function3<Boolean, Boolean, Boolean, Boolean>() {
                            @Override
                            public Boolean apply(Boolean emailValid, Boolean passwordValid, Boolean usernameExists) throws Exception {
                                Log.d(TAG, emailValid + " " + passwordValid + " " + usernameExists);
                                return emailValid && passwordValid && !usernameExists;
                            }
                        })
                          .subscribe(new Consumer<Boolean>() {
                              @Override
                              public void accept(Boolean enabled) throws Exception {
                                  mEmailSignInButton.setEnabled(enabled);
                              }
                          }));
        emailViewWatcherSubject.onNext(email);
    }

    private void attemptRegister() {

        if (mAuth == null) {
            return;
        }
        mEmailSignInButton.setEnabled(false);
        // Store values at the time of the register attempt.
        String email = mEmailView.getText().toString().trim();
        String password = mPasswordView.getText().toString();
        final String username = mUsernameView.getText().toString().trim();

        mProgressView.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Log.d(TAG, "onComplete: ", task.getException());
                    mProgressView.setVisibility(View.GONE);
                    mEmailSignInButton.setEnabled(true);
                    Snackbar.make(mEmailView, R.string.registration_failed, Snackbar.LENGTH_INDEFINITE).show();
                    return;
                }
                FirebaseUser firebaseUser = task.getResult().getUser();
                final UserEntity user = new UserEntity();
                user.setName(username);
                user.setServerKey(firebaseUser.getUid());
                user.setCurrentUser(true);
                // Saving on firebase db
                mDisposables.add(
                        mFirebaseClient
                                .addUser(user)
                                .doOnSuccess(new Consumer<User>() {
                                    @Override
                                    public void accept(User user) throws Exception {
                                        Log.d(TAG, "user written to firebase db");
                                    }
                                })
                                .doOnError(new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        Log.e(TAG, "error in writing to firebase db", throwable);
                                        deleteUserAccount();
                                    }
                                })
                                // Saving to local db
                                .flatMap(new Function<User, Single<User>>() {
                                    @Override
                                    public Single<User> apply(User user) throws Exception {
                                        return mDatabaseClient.saveUser(user);
                                    }
                                })
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<User>() {
                                    @Override
                                    public void onSuccess(User user) {
                                        Log.d(TAG, "User written to local db: " + user.getId());
                                        mProgressView.setVisibility(View.GONE);
                                        Intent conversationActivityIntent =
                                                new Intent(LoginActivity.this, ConversationsActivity.class);
                                        startActivity(conversationActivityIntent);
                                        finish();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(TAG, "Error writing to local db", e);
                                        deleteUserAccount();
                                    }
                                }));
            }
        });
    }

    private void deleteUserAccount() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mFirebaseClient.deleteAccount(currentUser);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                LoginActivity.this.requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                            }
                        }
                    });
        }
        else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDisposables.clear();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{
                ContactsContract.CommonDataKinds.Email
                        .CONTENT_ITEM_TYPE
        },

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
        addEmailsToAutoComplete(emails);
        cursor.close();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private interface ProfileQuery {

        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
                };
        int      ADDRESS    = 0;
        int      IS_PRIMARY = 1;
    }
}