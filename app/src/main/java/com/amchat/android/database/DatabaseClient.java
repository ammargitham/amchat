package com.amchat.android.database;

import com.amchat.android.models.AmChatFirebaseInstanceId;
import com.amchat.android.models.AmChatFirebaseInstanceIdEntity;
import com.amchat.android.models.Chat;
import com.amchat.android.models.ChatEntity;
import com.amchat.android.models.Conversation;
import com.amchat.android.models.ConversationEntity;
import com.amchat.android.models.User;
import com.amchat.android.models.UserEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.MaybeEmitter;
import io.reactivex.MaybeOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.functions.Function;
import io.requery.Persistable;
import io.requery.query.Tuple;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

/**
 * Created by Ammar Githam on 03/03/2017.
 */
public class DatabaseClient {

    private static final String TAG = "DatabaseClient";
    private ReactiveEntityStore<Persistable> mDataStore;

    public DatabaseClient(ReactiveEntityStore<Persistable> dataStore) {
        mDataStore = dataStore;
    }

    public Single<List<Conversation>> getConversations() {
        return Single.create(new SingleOnSubscribe<List<Conversation>>() {
            @Override
            public void subscribe(SingleEmitter<List<Conversation>> e) throws Exception {
                List<Conversation> conversations = mDataStore.select(Conversation.class)
                                                             .join(Chat.class)
                                                             .on(ConversationEntity.LAST_CHAT_ID.eq(ChatEntity.ID))
                                                             .orderBy(ChatEntity.ID.desc())
                                                             .get()
                                                             .toList();
                e.onSuccess(conversations);
            }
        });
    }

    public Single<User> saveUser(User user) {
        return mDataStore.insert(user);
    }

    public Single<User> saveUser(String userId, String username) {
        UserEntity userEntity = new UserEntity();
        userEntity.setName(userId);
        userEntity.setServerKey(username);
        return saveUser(userEntity);
    }

    public Single<Chat> saveChat(Chat chat) {
        return mDataStore.insert(chat);
    }

    public Maybe<User> getUser(final String userUid) {
        return Maybe.create(new MaybeOnSubscribe<User>() {
            @Override
            public void subscribe(MaybeEmitter<User> e) throws Exception {
                User user = mDataStore.select(User.class)
                                      .where(UserEntity.SERVER_KEY.eq(userUid))
                                      .get()
                                      .firstOrNull();
                if (user != null) {
                    e.onSuccess(user);
                    return;
                }
                e.onComplete();
            }
        });
    }

    public Maybe<User> getCurrentUser() {
        return Maybe.create(new MaybeOnSubscribe<User>() {
            @Override
            public void subscribe(MaybeEmitter<User> e) throws Exception {
                User user = mDataStore.select(User.class)
                                      .where(UserEntity.CURRENT_USER.eq(true))
                                      .get()
                                      .firstOrNull();
                if (user != null) {
                    e.onSuccess(user);
                    return;
                }
                e.onComplete();
            }
        });
    }

    public Maybe<Conversation> getConversationForUser(final long userId) {
        return Maybe.create(new MaybeOnSubscribe<Conversation>() {
            @Override
            public void subscribe(MaybeEmitter<Conversation> e) throws Exception {
                Conversation conversation = mDataStore.select(Conversation.class)
                                                      .where(ConversationEntity.USER_ID.eq(userId))
                                                      .get()
                                                      .firstOrNull();
                if (conversation != null) {
                    e.onSuccess(conversation);
                    return;
                }
                e.onComplete();
            }
        });
    }

    public Single<Conversation> newConversation(final long userId, final String userServerKey) {
        // If conversation with user already exists, return that conversation
        return getConversationForUser(userId)
                .switchIfEmpty(Maybe.fromSingle(actualNewConversation(userId, userServerKey)))
                .toSingle();
    }

    private Single<Conversation> actualNewConversation(long userId, String userServerKey) {
        ConversationEntity conversation = new ConversationEntity();
        conversation.setUserId(userId);
        conversation.setServerKey(userServerKey);
        return mDataStore.insert((Conversation) conversation);
    }

    public Single<List<Chat>> getChatsForConversation(final long conversationId) {
        return Single.create(new SingleOnSubscribe<List<Chat>>() {
            @Override
            public void subscribe(SingleEmitter<List<Chat>> e) throws Exception {
                List<Chat> chats = mDataStore.select(Chat.class)
                                             .where(ChatEntity.CONVERSATION_ID.eq(conversationId))
                                             .orderBy(ChatEntity.ID.asc())
                                             .get()
                                             .toList();
                //Log.d(TAG, "getChatsForConversation: chats: " + chats);
                e.onSuccess(chats);
            }
        });
    }

    public Observable<ReactiveResult<Chat>> getChatsObserverForConversation(long conversationId) {

        return mDataStore.select(Chat.class)
                         .where(ChatEntity.CONVERSATION_ID.eq(conversationId))
                         .orderBy(ChatEntity.ID.desc())
                         .limit(1)
                         .get()
                         .observableResult();
    }

    public Observable<ReactiveResult<Chat>> getChatsObserver() {

        return mDataStore.select(Chat.class)
                         .orderBy(ChatEntity.ID.desc())
                         .limit(1)
                         .get()
                         .observableResult();
    }

    public Maybe<User> getUser(final long userId) {
        return Maybe.create(new MaybeOnSubscribe<User>() {
            @Override
            public void subscribe(MaybeEmitter<User> e) throws Exception {
                User user = mDataStore.select(User.class)
                                      .where(UserEntity.ID.eq(userId))
                                      .get()
                                      .firstOrNull();
                if (user != null) {
                    e.onSuccess(user);
                    return;
                }
                e.onComplete();
            }
        });
    }

    public Maybe<Chat> getLastChat(final long conversationId) {
        return Maybe.create(new MaybeOnSubscribe<Chat>() {
            @Override
            public void subscribe(MaybeEmitter<Chat> e) throws Exception {
                Chat chat = mDataStore.select(Chat.class)
                                      .where(ChatEntity.CONVERSATION_ID.eq(conversationId))
                                      .orderBy(ChatEntity.ID.desc())
                                      .get()
                                      .firstOrNull();
                if (chat != null) {
                    e.onSuccess(chat);
                    return;
                }
                e.onComplete();
            }
        });
    }

    public Single<Integer> setLastChat(long conversationId, long chatId) {
        return mDataStore.update(Conversation.class)
                         .set(ConversationEntity.LAST_CHAT_ID, chatId)
                         .where(ConversationEntity.ID.eq(conversationId))
                         .get()
                         .single();
    }

    public Observable<ReactiveResult<Conversation>> getConversationsObserver() {
        return mDataStore.select(Conversation.class)
                         .join(Chat.class)
                         .on(ConversationEntity.LAST_CHAT_ID.eq(ChatEntity.ID))
                         .orderBy(ChatEntity.ID.desc())
                         .get()
                         .observableResult();
    }

    public Single<AmChatFirebaseInstanceIdEntity> updateInstanceId(String token) {
        AmChatFirebaseInstanceIdEntity instanceId = new AmChatFirebaseInstanceIdEntity();
        instanceId.setInstanceId(token);
        return mDataStore.upsert(instanceId);
    }

    public Maybe<String> getInstanceId() {
        return Maybe.create(new MaybeOnSubscribe<String>() {
            @Override
            public void subscribe(MaybeEmitter<String> e) throws Exception {
                AmChatFirebaseInstanceId instanceId = mDataStore.select(AmChatFirebaseInstanceId.class)
                                                                .get()
                                                                .firstOrNull();
                if (instanceId != null) {
                    e.onSuccess(instanceId.getInstanceId());
                    return;
                }
                e.onComplete();
            }
        });
    }

    public ReactiveResult<Chat> getUnreadChats() {
        return mDataStore.select(Chat.class)
                         .where(ChatEntity.STATUS.eq(Chat.STATUS_RECEIVED_UNREAD))
                         .orderBy(ChatEntity.ID.asc())
                         .get();
    }

    public Single<Integer> markChatAsRead(List<Long> chatIds) {
        return mDataStore.update(Chat.class)
                         .set(ChatEntity.STATUS, Chat.STATUS_RECEIVED_READ)
                         .where(ChatEntity.ID.in(chatIds))
                         .get()
                         .single();
    }

    public Single<Integer> getUnreadChatCountForConversation(long conversationId) {
        return mDataStore.count(Chat.class)
                         .where(ChatEntity.STATUS.eq(Chat.STATUS_RECEIVED_UNREAD)
                                                 .and(ChatEntity.CONVERSATION_ID.eq(conversationId)))
                         .get()
                         .single();
    }

    public Maybe<Conversation> getConversation(final long conversationId) {
        return Maybe.create(new MaybeOnSubscribe<Conversation>() {
            @Override
            public void subscribe(MaybeEmitter<Conversation> e) throws Exception {
                Conversation conversation = mDataStore.select(Conversation.class)
                                                      .where(ConversationEntity.ID.eq(conversationId))
                                                      .get()
                                                      .firstOrNull();
                if (conversation != null) {
                    e.onSuccess(conversation);
                    return;
                }
                e.onComplete();
            }
        });
    }

    public Single<List<String>> getChatKeysFromIds(final List<Long> chatIds) {
        return Single.create(new SingleOnSubscribe<List<String>>() {
            @Override
            public void subscribe(SingleEmitter<List<String>> e) throws Exception {
                List<Tuple> results = mDataStore.select(ChatEntity.SERVER_KEY)
                                                .distinct()
                                                .where(ChatEntity.ID.in(chatIds))
                                                .get()
                                                .toList();
                List<String> chatKeys = new ArrayList<>(results.size());
                for (Tuple result : results) {
                    chatKeys.add(result.get(0).toString());
                }
                e.onSuccess(chatKeys);
            }
        });
    }

    public Single<Chat> updateChatStatus(String messageKey, int status) {
        // Updating the chat
        mDataStore.update(Chat.class)
                  .set(ChatEntity.STATUS, status)
                  .where(ChatEntity.SERVER_KEY.eq(messageKey))
                  .get()
                  .value();
        return getChat(messageKey);
    }

    private Single<Chat> getChat(String messageKey) {
        return Single.fromObservable(
                mDataStore.select(Chat.class)
                          .where(ChatEntity.SERVER_KEY.eq(messageKey))
                          .get()
                          .observable());
    }

    public Single<Chat> updateChatServerKey(Chat chat, final String key) {
        // Updating the chat
        return mDataStore.update(Chat.class)
                         .set(ChatEntity.SERVER_KEY, key)
                         .where(ChatEntity.ID.eq(chat.getId()))
                         .get()
                         .single()
                         .flatMap(new Function<Integer, Single<Chat>>() {
                             @Override
                             public Single<Chat> apply(Integer integer) throws Exception {
                                 return getChat(key);
                             }
                         });
    }

    public Single<Integer> deleteChats(List<Long> ids) {
        return mDataStore.delete(Chat.class)
                         .where(ChatEntity.ID.in(ids))
                         .get()
                         .single();
    }
}
