package com.amchat.android.conversations;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amchat.android.R;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.models.Chat;
import com.amchat.android.models.Conversation;
import com.amchat.android.models.User;

import java.util.List;

/**
 * Created by Ammar Githam on 03/03/2017.
 */
class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {

    private DatabaseClient        mDatabaseClient;
    private List<Conversation>    mConversations;
    private OnAvatarClickListener mAvatarClickListener;

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View     mAvatarContainer;
        TextView mNameTextView;
        TextView mLastChatTextView;
        TextView mTimestampTextView;
        TextView mUnreadCountTextView;
        View     mUnreadIndicator;

        ViewHolder(ConstraintLayout v) {
            super(v);
            mAvatarContainer = v.findViewById(R.id.avatar);
            mAvatarContainer.setOnClickListener(this);
            mNameTextView = (TextView) v.findViewById(R.id.name);
            mLastChatTextView = (TextView) v.findViewById(R.id.lastChat);
            mTimestampTextView = (TextView) v.findViewById(R.id.timestamp);
            mUnreadCountTextView = (TextView) v.findViewById(R.id.unread_count);
            mUnreadIndicator = v.findViewById(R.id.unread_indicator);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.avatar:
                    if (mAvatarClickListener != null) {
                        mAvatarClickListener.onAvatarClick(v, getAdapterPosition());
                    }
                    break;
            }
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    ConversationsAdapter(List<Conversation> conversations, DatabaseClient databaseClient) {
        mConversations = conversations;
        mDatabaseClient = databaseClient;
    }

    @Override
    public ConversationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
            int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                                                              .inflate(R.layout.content_conversation_list_item, parent, false);
        return new ViewHolder(v);
    }

    interface OnAvatarClickListener {

        void onAvatarClick(View view, int position);
    }

    void setOnAvatarClickListener(OnAvatarClickListener avatarClickListener) {
        this.mAvatarClickListener = avatarClickListener;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        long conversationId = mConversations.get(position).getId();
        long userId = mConversations.get(position).getUserId();
        long unreadChatCount = mDatabaseClient.getUnreadChatCountForConversation(conversationId).blockingGet();
        Chat chat = mDatabaseClient.getLastChat(conversationId).blockingGet();
        final User user = mDatabaseClient.getUser(userId).blockingGet();
        if (user != null) {
            holder.mNameTextView.setText(user.getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View itemView) {
                    Intent chatIntent = new Intent(itemView.getContext(), ChatActivity.class);
                    chatIntent.putExtra(ChatActivity.USER_UID, user.getServerKey());
                    chatIntent.putExtra(ChatActivity.USER_NAME, user.getName());
                    itemView.getContext().startActivity(chatIntent);
                }
            });
        }
        if (chat != null) {
            holder.mTimestampTextView.setText(
                    DateUtils.getRelativeTimeSpanString(holder.itemView.getContext(), chat.getTimestamp(), false));
            holder.mLastChatTextView.setText(chat.getChatString());
        }
        if (unreadChatCount > 0) {
            holder.mUnreadIndicator.setVisibility(View.VISIBLE);
            holder.mUnreadCountTextView.setText(String.valueOf(unreadChatCount));
            ((View) holder.mUnreadCountTextView.getParent()).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.mUnreadIndicator.setVisibility(View.GONE);
        ((View) holder.mUnreadCountTextView.getParent()).setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }
}
