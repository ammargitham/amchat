package com.amchat.android.conversations;

/**
 * Created by Ammar Githam on 22/03/2017.
 */
public class SearchResult {

    public static final int TYPE_USER = 1;
    private String id;
    private String name;
    private int    type;

    public String getId() {
        return id;
    }

    public SearchResult setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public SearchResult setName(String name) {
        this.name = name;
        return this;
    }

    public int getType() {
        return type;
    }

    public SearchResult setType(int type) {
        this.type = type;
        return this;
    }
}
