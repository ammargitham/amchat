package com.amchat.android.conversations;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amchat.android.R;

import java.util.List;

/**
 * Created by Ammar Githam on 03/03/2017.
 */
class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder> {

    private static final String TAG = "SearchResultsAdapter";
    private List<SearchResult>  mSearchResults;
    private String              mQueryString;
    private OnItemClickListener mItemClickListener;

    void setQueryString(String queryString) {
        mQueryString = queryString;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mNameTextView;

        ViewHolder(ConstraintLayout v) {
            super(v);
            mNameTextView = (TextView) v.findViewById(R.id.name);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

    void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    SearchResultsAdapter(List<SearchResult> searchResults) {
        mSearchResults = searchResults;
    }

    @Override
    public SearchResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                                                              .inflate(R.layout.content_search_result_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String name = mSearchResults.get(position).getName();
        SpannableString nameSpannableString = new SpannableString(name);
        if (mQueryString != null) {
            int start = name.indexOf(mQueryString);
            int end = start + mQueryString.length();
            nameSpannableString.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            nameSpannableString.setSpan(new BackgroundColorSpan(Color.GRAY), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        holder.mNameTextView.setText(nameSpannableString);
    }

    @Override
    public int getItemCount() {
        return mSearchResults.size();
    }
}
