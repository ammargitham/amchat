package com.amchat.android.conversations;

import com.amchat.android.models.Chat;

/**
 * Created by Ammar Githam on 29/03/2017.
 */
class ChatAdapterItem {

    private static final String TAG                 = "ChatAdapterItem";
    static final         int    TYPE_UNREAD_HEADER  = 1;
    static final         int    TYPE_DATE           = 2;
    static final         int    TYPE_CHAT           = 3;
    static final         int    TYPE_INFO           = 4;
    /*static final         int    TYPE_MESSAGE_STATUS = 5;*/
    private              int    type                = -1;
    private Object item;

    ChatAdapterItem(int type, Object item) {
        switch (type) {
            case TYPE_UNREAD_HEADER:
                if (!(item instanceof Integer)) {
                    throw new IllegalArgumentException("TYPE_UNREAD_HEADER requires an Integer item");
                }
                break;
            case TYPE_DATE:
                if (!(item instanceof String)) {
                    throw new IllegalArgumentException("TYPE_DATE requires a String item");
                }
                break;
            case TYPE_CHAT:
                if (!(item instanceof Chat)) {
                    throw new IllegalArgumentException("TYPE_CHAT requires a Chat item");
                }
                break;
            case TYPE_INFO:
                if (!(item instanceof String)) {
                    throw new IllegalArgumentException("TYPE_INFO requires a String item");
                }
                break;
            /*case TYPE_MESSAGE_STATUS:
                if (!(item instanceof String)) {
                    throw new IllegalArgumentException("TYPE_MESSAGE_STATUS requires a String item");
                }
                break;*/
            default:
                throw new IllegalArgumentException("Type " + type + " is invalid");
        }
        this.type = type;
        this.item = item;
    }

    public int getType() {
        return type;
    }

    private Class getTypeClass() {
        switch (type) {
            case TYPE_UNREAD_HEADER:
                return Integer.class;
            case TYPE_DATE:
                return String.class;
            case TYPE_CHAT:
                return Chat.class;
            case TYPE_INFO:
                return String.class;
            /*case TYPE_MESSAGE_STATUS:
                return String.class;*/
        }
        return null;
    }

    Object getItem() {
        // return the item else return null
        switch (type) {
            case TYPE_UNREAD_HEADER:
            case TYPE_DATE:
            case TYPE_CHAT:
            case TYPE_INFO:
            /*case TYPE_MESSAGE_STATUS:*/
                return item;
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ChatAdapterItem)) {
            return false;
        }
        ChatAdapterItem chatAdapterItem = (ChatAdapterItem) obj;
        if (getType() != chatAdapterItem.getType()) {
            return false;
        }
        //Log.d(TAG, "equals: getTypeClass(): " + getTypeClass() + ", chatAdapterItem.getTypeClass(): " + chatAdapterItem.getTypeClass());
        int objType = chatAdapterItem.getType();
        switch (objType) {
            // If its a chat, id equality will be sufficient
            case TYPE_CHAT:
                Chat chat = (Chat) chatAdapterItem.getItem();
                Chat thisChat = (Chat) getItem();
                return chat.getId() == thisChat.getId();
        }
        return false;
    }
}
