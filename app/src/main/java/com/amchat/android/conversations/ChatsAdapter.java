package com.amchat.android.conversations;

import android.os.Parcelable;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amchat.android.R;
import com.amchat.android.models.Chat;
import com.amchat.android.utils.MultiChoiceHelper;

import java.util.List;

/**
 * Created by Ammar Githam on 03/03/2017.
 */
class ChatsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ChatsAdapter";
    private final MultiChoiceHelper     mMultiChoiceHelper;
    private       List<ChatAdapterItem> mChatAdapterItems;
    private       long                  mCurrentUserId;

    private static class ChatItemViewHolder extends MultiChoiceHelper.ViewHolder {

        TextView mChatTextView;
        TextView mTimestampTextView;
        TextView mStatusTextView;

        ChatItemViewHolder(LinearLayout v) {
            super(v);
            mChatTextView = (TextView) v.findViewById(R.id.chat);
            mTimestampTextView = (TextView) v.findViewById(R.id.timestamp);
            mStatusTextView = (TextView) v.findViewById(R.id.status);
        }
    }

    private static class UnreadHeaderItemViewHolder extends RecyclerView.ViewHolder {

        TextView mUnreadHeaderTextView;

        UnreadHeaderItemViewHolder(LinearLayout v) {
            super(v);
            mUnreadHeaderTextView = (TextView) v.findViewById(R.id.unread_header);
        }
    }

    private static class DateItemViewHolder extends RecyclerView.ViewHolder {

        TextView mDateTextView;

        DateItemViewHolder(LinearLayout v) {
            super(v);
            mDateTextView = (TextView) v.findViewById(R.id.date);
        }
    }

    /*private static class MessageStatusItemViewHolder extends RecyclerView.ViewHolder {

        TextView mStatusTextView;

        MessageStatusItemViewHolder(LinearLayout v) {
            super(v);
            mStatusTextView = (TextView) v.findViewById(R.id.status);
        }
    }*/

    ChatsAdapter(long currentUserId, List<ChatAdapterItem> chatAdapterItems, final ChatActivity activity) {
        mCurrentUserId = currentUserId;
        mChatAdapterItems = chatAdapterItems;
        setHasStableIds(true);
        mMultiChoiceHelper = new MultiChoiceHelper(activity, this);
        mMultiChoiceHelper.setMultiChoiceModeListener(new MultiChoiceHelper.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                updateSelectedCountDisplay(mode);
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.action_mode_chat_selection, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                updateSelectedCountDisplay(mode);
                return true;
            }

            private void updateSelectedCountDisplay(ActionMode mode) {
                int count = mMultiChoiceHelper.getCheckedItemCount();
                mode.setTitle(mMultiChoiceHelper.getContext().getResources().getQuantityString(R.plurals.selected_chats, count, count));
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        SparseBooleanArray checkedItemPositions = mMultiChoiceHelper.getCheckedItemPositions();
                        long[] checkedItemIds = mMultiChoiceHelper.getCheckedItemIds();
                        activity.deleteChats(checkedItemIds, checkedItemPositions);
                        return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) { }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ChatAdapterItem.TYPE_CHAT:
                LinearLayout chatView = (LinearLayout) LayoutInflater.from(parent.getContext())
                                                                     .inflate(R.layout.content_chat_item, parent, false);
                viewHolder = new ChatItemViewHolder(chatView);
                break;
            case ChatAdapterItem.TYPE_UNREAD_HEADER:
                LinearLayout unreadHeaderView = (LinearLayout) LayoutInflater.from(parent.getContext())
                                                                             .inflate(R.layout.content_unread_header_item, parent, false);
                viewHolder = new UnreadHeaderItemViewHolder(unreadHeaderView);
                break;
            case ChatAdapterItem.TYPE_DATE:
                LinearLayout dateView = (LinearLayout) LayoutInflater.from(parent.getContext())
                                                                     .inflate(R.layout.content_date_item, parent, false);
                viewHolder = new DateItemViewHolder(dateView);
                break;
            /*case ChatAdapterItem.TYPE_MESSAGE_STATUS:
                LinearLayout statusView = (LinearLayout) LayoutInflater.from(parent.getContext())
                                                                       .inflate(R.layout.content_message_status_item, parent, false);
                viewHolder = new MessageStatusItemViewHolder(statusView);
                break;*/
        }
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return mChatAdapterItems.get(position).getType();
    }

    @Override
    public long getItemId(int position) {
        ChatAdapterItem chatAdapterItem = mChatAdapterItems.get(position);
        if (chatAdapterItem.getType() == ChatAdapterItem.TYPE_CHAT) {
            return ((Chat) chatAdapterItem.getItem()).getId();
        }
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ChatAdapterItem.TYPE_CHAT:
                Chat chat = (Chat) mChatAdapterItems.get(position).getItem();
                ChatItemViewHolder chatViewHolder = (ChatItemViewHolder) holder;
                chatViewHolder.mChatTextView.setText(chat.getChatString());
                chatViewHolder.mTimestampTextView.setText(
                        DateUtils.getRelativeTimeSpanString(holder.itemView.getContext(), chat.getTimestamp(), false));
                if (chat.getUserId() == mCurrentUserId) {
                    ((LinearLayout) holder.itemView).setGravity(Gravity.RIGHT);
                    chatViewHolder.mStatusTextView.setVisibility(View.VISIBLE);
                    chatViewHolder.mStatusTextView.setText(getMessageStatusString(chat.getStatus()));
                }
                else {
                    ((LinearLayout) holder.itemView).setGravity(Gravity.LEFT);
                }
                // Enable MultiChoice selection and update checked state
                chatViewHolder.bind(mMultiChoiceHelper, position);
                break;
            case ChatAdapterItem.TYPE_UNREAD_HEADER:
                int unreadCount = (int) mChatAdapterItems.get(position).getItem();
                ((UnreadHeaderItemViewHolder) holder).mUnreadHeaderTextView.setText(
                        holder.itemView.getResources().getQuantityString(R.plurals.number_of_unread_chats, unreadCount, unreadCount));
                break;
            case ChatAdapterItem.TYPE_DATE:
                String dateString = (String) mChatAdapterItems.get(position).getItem();
                ((DateItemViewHolder) holder).mDateTextView.setText(dateString);
                break;
            /*case ChatAdapterItem.TYPE_MESSAGE_STATUS:
                String statusString = (String) mChatAdapterItems.get(position).getItem();
                ((MessageStatusItemViewHolder) holder).mStatusTextView.setText(statusString);
                break;*/
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        switch (holder.getItemViewType()) {
            case ChatAdapterItem.TYPE_CHAT:
                ((ChatItemViewHolder) holder).mStatusTextView.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mChatAdapterItems.size();
    }

    private String getMessageStatusString(int status) {
        String statusString = "";
        switch (status) {
            case Chat.STATUS_SENT_PENDING:
                statusString = "Sending...";
                break;
            case Chat.STATUS_SENT_SENT:
                statusString = "Sent.";
                break;
            case Chat.STATUS_SENT_DELIVERED:
                statusString = "Delivered.";
                break;
            case Chat.STATUS_SENT_READ:
                statusString = "Read.";
                break;
        }
        return statusString;
    }

    public Parcelable onSaveInstanceState() {
        return mMultiChoiceHelper.onSaveInstanceState();
    }

    public void onRestoreInstanceState(Parcelable state) {
        mMultiChoiceHelper.onRestoreInstanceState(state);
    }

    public void onDestroyView() {
        mMultiChoiceHelper.clearChoices();
    }
}
