package com.amchat.android.conversations;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amchat.android.AmchatApplication;
import com.amchat.android.R;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.firebase.FirebaseClient;
import com.amchat.android.models.Chat;
import com.amchat.android.models.ChatEntity;
import com.amchat.android.models.Conversation;
import com.amchat.android.models.User;
import com.amchat.android.services.MessageEvent;
import com.amchat.android.utils.Utilities;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.requery.reactivex.ReactiveResult;

public class ChatActivity extends AppCompatActivity {

    private static final String              TAG                   = "ChatActivity";
    public static final  String              USER_NAME             = "username";
    public static final  String              USER_UID              = "user_id";
    private final        CompositeDisposable mDisposables          = new CompositeDisposable();
    private              int                 mUnreadHeaderPosition = -1;
    /*private              int                 mMessageSentPosition      = -1;
    private              int                 mMessageDeliveredPosition = -1;
    private              int                 mMessageReadPosition      = -1;*/
    private List<ChatAdapterItem> mChatAdapterItems;
    private ChatsAdapter          mChatsAdapter;
    private Conversation          mConversation;
    private RecyclerView          mChatsRecyclerView;
    private EditText              mInputEditText;
    private FirebaseClient        mFirebaseClient;
    private DatabaseClient        mDatabaseClient;
    private User                  mRecipientUser;
    private User                  mCurrentUser;
    private LinearLayoutManager   mLayoutManager;
    private List<Long>            mUnreadChatIds;
    private boolean               mIsActivityVisible;
    private boolean               mAreChatsFetched;
    private String                mLastDateString;
    private Date                  mCurrentDate;
    /*private int                   mMessageSentStatus;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //getWindow().requestFeature(Window.FEATURE_ACTION_MODE_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //long conversationId = getIntent().getLongExtra(CONVERSATION_ID, -1);
        String userUid = getIntent().getStringExtra(USER_UID);
        String username = getIntent().getStringExtra(USER_NAME);

        Log.d(TAG, "onCreate: userUid: " + userUid + " username: " + username);
        if (userUid == null || username == null) {
            Toast.makeText(ChatActivity.this, "Something was null!", Toast.LENGTH_LONG).show();
            finish();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(username);
        }

        mChatsRecyclerView = (RecyclerView) findViewById(R.id.chats_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setStackFromEnd(true);
        mChatsRecyclerView.setLayoutManager(mLayoutManager);
        SimpleItemDecorator simpleItemDecorator = new SimpleItemDecorator((int) Utilities.convertDpToPixel(8, mChatsRecyclerView.getContext()));
        mChatsRecyclerView.addItemDecoration(simpleItemDecorator);
        mInputEditText = (EditText) findViewById(R.id.input_edit_text);
        mInputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    sendChat();
                    return true;
                }
                return false;
            }
        });
        mChatAdapterItems = new ArrayList<>();
        mFirebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
        mDatabaseClient = new DatabaseClient(((AmchatApplication) getApplication()).getData());
        mUnreadChatIds = new ArrayList<>();

        // Current date for date headers
        mCurrentDate = new Date();
        //getRecipientUser(userUid, username);
        getRecipientAndCurrentUser(userUid, username);
    }

    private void getRecipientAndCurrentUser(final String recipientUserUid, final String recipientUsername) {
        mDisposables.add(
                mDatabaseClient.getCurrentUser()
                               .doOnSuccess(new Consumer<User>() {
                                   @Override
                                   public void accept(User user) throws Exception {
                                       Log.d(TAG, "accept: got current user: " + user);
                                       mCurrentUser = user;
                                   }
                               })
                               .flatMap(new Function<User, Maybe<User>>() {
                                   @Override
                                   public Maybe<User> apply(User user) throws Exception {
                                       return mDatabaseClient.getUser(recipientUserUid);
                                   }
                               })
                               .doOnComplete(new Action() {
                                   @Override
                                   public void run() throws Exception {
                                       // user not found from db, add it using the available info
                                       Log.d(TAG, "apply: recipient user not found in local db, saving with available user info");
                                   }
                               })
                               .switchIfEmpty(Maybe.fromSingle(mDatabaseClient.saveUser(recipientUserUid, recipientUsername)))
                               .toSingle()
                               .doOnSuccess(new Consumer<User>() {
                                   @Override
                                   public void accept(User user) throws Exception {
                                       Log.d(TAG, "accept: got recipient user: " + user);
                                       mRecipientUser = user;
                                   }
                               })
                               .subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribeWith(new DisposableSingleObserver<User>() {
                                   @Override
                                   public void onSuccess(User user) {
                                       mChatsAdapter = new ChatsAdapter(mCurrentUser.getId(), mChatAdapterItems, ChatActivity.this);
                                       mChatsRecyclerView.setAdapter(mChatsAdapter);
                                       fetchChats();
                                       //setChatsObserver();
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       Log.e(TAG, "onError: Error in getting users", e);
                                       finish();
                                   }
                               }));
    }

    private void sendChat() {

        DisposableSingleObserver<Chat> disposableObserver =
                Single.fromCallable(
                        new Callable<Conversation>() {
                            @Override
                            public Conversation call() throws Exception {
                                if (mConversation == null) {
                                    mConversation = mDatabaseClient.newConversation(mRecipientUser.getId(), mRecipientUser.getServerKey())
                                                                   .blockingGet();
                                }
                                return mConversation;
                            }
                        })
                      .flatMap(new Function<Conversation, Single<Chat>>() {
                          @Override
                          public Single<Chat> apply(Conversation conversation) throws Exception {
                              String chatString = mInputEditText.getText().toString().trim();
                              ChatEntity chat = new ChatEntity();
                              chat.setTimestamp(new Date().getTime());
                              chat.setChatString(chatString);
                              chat.setConversationId(conversation.getId());
                              chat.setStatus(Chat.STATUS_SENT_PENDING);
                              // The user of the chat object will be the current user
                              // It will be sent to the recipient user.
                              // Currently there is no need to save the recipient user in the chat object.
                              chat.setUserId(mCurrentUser.getId());
                              // Write a message to the local database
                              return mDatabaseClient.saveChat(chat);
                          }
                      })
                      .doOnSuccess(new Consumer<Chat>() {
                          @Override
                          public void accept(Chat chat) throws Exception {
                              // Update last chat in conversation
                              mDatabaseClient.setLastChat(mConversation.getId(), chat.getId());
                          }
                      })
                      .observeOn(AndroidSchedulers.mainThread()) //Changing to main thread to update ui
                      .doOnSuccess(new Consumer<Chat>() {
                          @Override
                          public void accept(Chat chat) throws Exception {
                              Log.d(TAG, "accept: saved chat to local db, id is: " + chat.getId());
                              mInputEditText.setText("");
                          }
                      })
                      .observeOn(Schedulers.io()) // Changing to io thread for network call
                      .map(new Function<Chat, Pair<Chat, String>>() {
                          @Override
                          public Pair<Chat, String> apply(Chat chat) throws Exception {
                              // Write to firebase database
                              String chatKey = mFirebaseClient.sendChat(chat, mCurrentUser, mRecipientUser).blockingGet();
                              return Pair.create(chat, chatKey);
                          }
                      })
                      .flatMap(new Function<Pair<Chat, String>, Single<Chat>>() {
                          @Override
                          public Single<Chat> apply(Pair<Chat, String> chatAndKeyPair) throws Exception {
                              Chat chat = chatAndKeyPair.first;
                              String key = chatAndKeyPair.second;
                              // Update server key in local db
                              mDatabaseClient.updateChatServerKey(chat, key).blockingGet();
                              // Update status in local db
                              return mDatabaseClient.updateChatStatus(key, Chat.STATUS_SENT_SENT);
                          }
                      })
                      .subscribeOn(Schedulers.io())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribeWith(new DisposableSingleObserver<Chat>() {
                          @Override
                          public void onSuccess(Chat chat) {
                              Log.d(TAG, "onSuccess: saved to firebase db: " + chat.getServerKey());
                          }

                          @Override
                          public void onError(Throwable e) {
                              Log.e(TAG, "onError: error saving chat to firebase db", e);
                          }
                      });
        mDisposables.add(disposableObserver);
    }

    private void fetchChats() {

        // Get all the current chats in this conversation
        mDisposables.add(
                // Check if conversation available with recipient user
                mDatabaseClient
                        .getConversationForUser(mRecipientUser.getId())
                        .doOnSuccess(new Consumer<Conversation>() {
                            @Override
                            public void accept(Conversation conversation) throws Exception {
                                //Log.d(TAG, "accept: got Conversation: " + conversation);
                            }
                        })
                        .switchIfEmpty(Maybe.fromSingle(mDatabaseClient.newConversation(mRecipientUser.getId(), mRecipientUser.getServerKey())))
                        .toSingle()
                        .flatMap(new Function<Conversation, Single<List<Chat>>>() {
                            @Override
                            public Single<List<Chat>> apply(Conversation conversation) throws Exception {
                                mConversation = conversation;
                                return mDatabaseClient.getChatsForConversation(mConversation.getId());
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<List<Chat>>() {
                            @Override
                            public void onSuccess(List<Chat> chats) {
                                for (Chat chat : chats) {
                                    String dateString = getDateString(chat.getTimestamp());
                                    if (mLastDateString == null || !mLastDateString.equals(dateString)) {
                                        addDateHeader(dateString);
                                        mLastDateString = dateString;
                                    }
                                    if (chat.getStatus() == Chat.STATUS_RECEIVED_UNREAD) {
                                        mUnreadChatIds.add(chat.getId());
                                    }
                                    ChatAdapterItem chatAdapterItem = new ChatAdapterItem(ChatAdapterItem.TYPE_CHAT, chat);
                                    mChatAdapterItems.add(chatAdapterItem);
                                  /*if (chat.getUserId() == mCurrentUser.getId()) {
                                      switch (chat.getStatus()) {
                                          case Chat.STATUS_RECEIVED_UNREAD:
                                              mUnreadChatIds.add(chat.getId());
                                              break;
                                          case Chat.STATUS_SENT_PENDING:
                                              addMessageStatus(Chat.STATUS_SENT_PENDING, mChatAdapterItems.size());
                                              mMessageSentStatus = Chat.STATUS_SENT_PENDING;
                                              break;
                                          case Chat.STATUS_SENT_SENT:
                                              addMessageStatus(Chat.STATUS_SENT_SENT, mChatAdapterItems.size());
                                              mMessageSentStatus = Chat.STATUS_SENT_SENT;
                                              break;
                                          case Chat.STATUS_SENT_DELIVERED:
                                              addMessageStatus(Chat.STATUS_SENT_DELIVERED, mChatAdapterItems.size());
                                              break;
                                          case Chat.STATUS_SENT_READ:
                                              addMessageStatus(Chat.STATUS_SENT_READ, mChatAdapterItems.size());
                                              break;
                                      }
                                  }*/
                                }
                                addUnreadHeader();
                                mChatsAdapter.notifyDataSetChanged();
                                mChatsRecyclerView.scrollToPosition(mChatAdapterItems.size() - 1);
                                // Marking unread chats as read
                                //Log.d(TAG, "onComplete: Marking read from fetchChats()");
                                markChatsAsRead();
                                // Setting chats observer to observe for any new chats
                                setChatsObserver();
                                mAreChatsFetched = true;
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: Error fetching chats", e);
                            }
                        })

        );
    }

    /*private void addMessageStatus(int status, int position) {

        int previousPosition = -1;
        String statusString = "";
        switch (status) {
            case Chat.STATUS_SENT_PENDING:
                previousPosition = mMessageSentPosition;
                mMessageSentPosition = position;
                statusString = "Sending...";
                break;
            case Chat.STATUS_SENT_SENT:
                previousPosition = mMessageSentPosition;
                mMessageSentPosition = position;
                statusString = "Sent.";
                break;
            case Chat.STATUS_SENT_DELIVERED:
                previousPosition = mMessageDeliveredPosition;
                mMessageDeliveredPosition = previousPosition;
                statusString = "Delivered.";
                break;
            case Chat.STATUS_SENT_READ:
                previousPosition = mMessageReadPosition;
                mMessageReadPosition = previousPosition;
                statusString = "Read.";
                break;
        }
        if (previousPosition != -1) {
            Log.d(TAG, "addMessageStatus: removing item at position: " + previousPosition);
            mChatAdapterItems.remove(previousPosition);
        }
        ChatAdapterItem chatAdapterItem = new ChatAdapterItem(ChatAdapterItem.TYPE_MESSAGE_STATUS, statusString);
        if (position >= mChatAdapterItems.size()) {
            Log.d(TAG, "addMessageStatus: status: " + statusString + " added at " + (mChatAdapterItems.size() - 1));
            mChatAdapterItems.add(chatAdapterItem);
        }
        else {
            Log.d(TAG, "addMessageStatus: status: " + statusString + " added at " + position);
            mChatAdapterItems.add(position, chatAdapterItem);
        }
    }*/

    @NonNull
    private String getDateString(long timestamp) {
        String dateString;
        Date chatDate = new Date(timestamp);
        long dateDiff = Utilities.getDateDiff(chatDate, mCurrentDate, TimeUnit.DAYS);
        //Log.d(TAG, "getDateString: dateDiff: " + dateDiff);
        if (dateDiff == 0) {
            dateString = "Today";
        }
        else if (dateDiff == 1) {
            dateString = "Yesterday";
        }
        else if (dateDiff <= 365) {
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM", Locale.getDefault());
            dateString = format.format(chatDate);
        }
        else {
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            dateString = format.format(chatDate);
        }
        return dateString;
    }

    private void addDateHeader(String dateString) {

        mChatAdapterItems.add(new ChatAdapterItem(ChatAdapterItem.TYPE_DATE, dateString));
    }

    private void markChatsAsRead() {
        if (mUnreadChatIds.isEmpty()) {
            return;
        }
        mDisposables.add(
                //Get chat keys from chat ids
                mDatabaseClient.getChatKeysFromIds(mUnreadChatIds)
                               // Mark read at firebase
                               .flatMapCompletable(new Function<List<String>, Completable>() {
                                   @Override
                                   public Completable apply(List<String> chatKeys) throws Exception {
                                       return mFirebaseClient.setChatStatus(mRecipientUser.getServerKey(), mCurrentUser.getServerKey(), chatKeys,
                                               Chat.STATUS_RECEIVED_READ);
                                   }
                               })
                               //Mark read at local db
                               .andThen(mDatabaseClient.markChatAsRead(mUnreadChatIds))
                               .subscribe(new Consumer<Integer>() {
                                   @Override
                                   public void accept(Integer count) throws Exception {
                                       Log.d(TAG, "accept: Updated " + count + " chats as read");
                                       mUnreadChatIds.clear();
                                   }
                               })
        );
    }

    private void setChatsObserver() {

        if (mConversation == null) {
            return;
        }
        mDisposables.add(
                mDatabaseClient
                        .getChatsObserverForConversation(mConversation.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnNext(new Consumer<ReactiveResult<Chat>>() {
                            @Override
                            public void accept(ReactiveResult<Chat> chats) throws Exception {
                                //Log.d(TAG, "setChatsObserver: accept: chats: " + chats.toList());
                            }
                        })
                        .subscribe(new Consumer<ReactiveResult<Chat>>() {
                            @Override
                            public void accept(ReactiveResult<Chat> chats) throws Exception {
                                List<Chat> chatList = chats.toList();
                                if (chatList.isEmpty()) {
                                    return;
                                }
                                // chats contains only 1 chat object
                                final Chat chat = chatList.get(0);
                                ChatAdapterItem chatAdapterItem = new ChatAdapterItem(ChatAdapterItem.TYPE_CHAT, chat);
                                int index = mChatAdapterItems.indexOf(chatAdapterItem);
                                if (index >= 0) {
                                    // If mChats already contains this chat just update it at its position.
                                    //Log.d(TAG, "accept: mChats contains chat: " + chat);
                                    mChatAdapterItems.set(index, chatAdapterItem);
                                    mChatsAdapter.notifyItemChanged(index);
                                    return;
                                }
                                /*if (chat.getUserId() == mCurrentUser.getId()) {
                                    addMessageStatus(chat.getStatus(), index + 1);
                                }*/
                                //Log.d(TAG, "accept: mChats does not contain: " + chat);
                                mChatAdapterItems.add(chatAdapterItem);
                                mChatsAdapter.notifyItemInserted(mChatAdapterItems.size() - 1);
                                if (mLayoutManager.findLastCompletelyVisibleItemPosition() == -1 ||
                                        mLayoutManager.findLastCompletelyVisibleItemPosition() == mChatAdapterItems.size() - 2) {
                                    // Scroll to show latest message
                                    mChatsRecyclerView.scrollToPosition(mChatAdapterItems.size() - 1);
                                }
                                // Marking as read
                                if (chat.getStatus() == Chat.STATUS_RECEIVED_UNREAD) {
                                    mUnreadChatIds.add(chat.getId());
                                    if (mIsActivityVisible) {
                                        // Only mark as read if window is visible
                                        //Log.d(TAG, "onComplete: Marking read from setChatsObserver()");
                                        markChatsAsRead();
                                    }
                                }
                                if (mAreChatsFetched && mIsActivityVisible) {
                                    //Log.d(TAG, "onResume: Removing header from setChatsObserver");
                                    removeUnreadHeader();
                                    mChatsRecyclerView.scrollToPosition(mChatAdapterItems.size() - 1);
                                }
                            }
                        }));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mIsActivityVisible = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Dismiss any unread message notification
        Utilities.dismissUnreadNotification(ChatActivity.this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mIsActivityVisible = true;
        // Do all these only if onResume not called after onCreate
        if (mAreChatsFetched) {
            if (mUnreadChatIds.isEmpty()) {
                // Remove the unread header
                //Log.d(TAG, "onResume: Removing header from onResume");
                removeUnreadHeader();
            }
            else {
                // Add unread header
                addUnreadHeader();
                // Mark chats as read on resume
                //Log.d(TAG, "onComplete: Marking read from onResume()");
                markChatsAsRead();
            }
        }
    }

    private void addUnreadHeader() {
        if (mUnreadChatIds.isEmpty()) {
            return;
        }
        // If there are any unread chats,
        // place the unread header before the first unread chat
        mUnreadHeaderPosition = mChatAdapterItems.size() - mUnreadChatIds.size();
        //Log.d(TAG, "addUnreadHeader: mUnreadHeaderPosition: " + mUnreadHeaderPosition);
        if (mUnreadHeaderPosition != -1) {
            // Add the header item to the adapter dataset
            mChatAdapterItems.add(mUnreadHeaderPosition,
                    new ChatAdapterItem(ChatAdapterItem.TYPE_UNREAD_HEADER, mUnreadChatIds.size()));
        }
    }

    private void removeUnreadHeader() {
        if (mUnreadHeaderPosition == -1) {
            return;
        }
        mChatAdapterItems.remove(mUnreadHeaderPosition);
        mChatsAdapter.notifyItemRemoved(mUnreadHeaderPosition);
        mUnreadHeaderPosition = -1;
    }

    @Subscribe(priority = 1)
    public void onMessageEvent(MessageEvent event) {
        if (event.fromUid.equals(mRecipientUser.getServerKey())) {
            //Log.d(TAG, "onMessageEvent: Got event in ChatActivity: " + event);
            EventBus.getDefault().cancelEventDelivery(event);
        }
    }

    @Override
    protected void onPause() {
        mIsActivityVisible = false;
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        removeUnreadHeader();
        super.onPause();
    }

    @Override
    public void onStop() {
        mIsActivityVisible = false;
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIsActivityVisible = false;
        // Clear the subscriptions/Observers
        mDisposables.clear();
    }

    public void deleteChats(final long[] checkedItemIds, final SparseBooleanArray checkedItemPositions) {
        List<Long> idList = new ArrayList<>(checkedItemIds.length);
        for (long id : checkedItemIds) {
            idList.add(id);
        }
        mDisposables.add(
                mDatabaseClient
                        .deleteChats(idList)
                        .map(new Function<Integer, Integer>() {
                            @Override
                            public Integer apply(Integer count) throws Exception {
                                for (int i = 0; i < checkedItemPositions.size(); i++) {
                                    if (checkedItemPositions.valueAt(i)) {
                                        mChatAdapterItems.remove(checkedItemPositions.keyAt(i));
                                    }
                                }
                                return count;
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<Integer>() {
                            @Override
                            public void onSuccess(Integer count) {
                                Log.d(TAG, "onComplete: Deleted " + count + " chats");
                                mChatsAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: ", e);
                            }
                        })
        );
    }
}
