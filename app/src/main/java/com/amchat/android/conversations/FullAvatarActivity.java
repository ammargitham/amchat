package com.amchat.android.conversations;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Path;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.transition.Fade;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;

import com.amchat.android.R;
import com.amchat.android.transitions.GravityArcMotion;

import static com.amchat.android.transitions.AnimUtils.getFastOutSlowInInterpolator;

public class FullAvatarActivity extends AppCompatActivity {

    private static final String TAG                = "FullAvatarActivity";
    static final         String EXTRA_START_X      = "start_x";
    static final         String EXTRA_START_Y      = "start_y";
    static final         String EXTRA_START_RADIUS = "start_radius";
    private int  mStartRadius;
    private View mAvatarFullView;
    private View mAvatarSmallView;
    private int  mStartX;
    private int  mStartY;
    private int  mStatusBarHeight;
    private int  mFabExpandDuration;
    private int  mTranslateX;
    private int  mTranslateY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        getWindow().setEnterTransition(new Fade());
        getWindow().setExitTransition(new Fade());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_avatar);
        mAvatarFullView = findViewById(R.id.avatar_full);
        mAvatarSmallView = findViewById(R.id.avatar);
        mStartX = getIntent().getIntExtra(EXTRA_START_X, 0);
        mStartY = getIntent().getIntExtra(EXTRA_START_Y, 0);
        mStartRadius = getIntent().getIntExtra(EXTRA_START_RADIUS, 0);
        mFabExpandDuration = getResources().getInteger(R.integer.fab_expand_duration);

        mAvatarSmallView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mAvatarSmallView.getViewTreeObserver().removeOnPreDrawListener(this);
                //Log.d(TAG, "onPreDraw: mStartRadius: " + mStartRadius + ", mStartX: " + mStartX + ", mStartY: " + mStartY);
                ViewGroup.LayoutParams layoutParams = mAvatarSmallView.getLayoutParams();
                layoutParams.height = mStartRadius;
                layoutParams.width = mStartRadius;
                mAvatarSmallView.setLayoutParams(layoutParams);
                mAvatarSmallView.setX(mStartX);
                mAvatarSmallView.setY(mStartY - getStatusBarHeight());
                mAvatarSmallView.setAlpha(0.5f);
                ((CardView) mAvatarSmallView).setRadius(mStartRadius / 2);
                doOpenAnimation();
                return true;
            }
        });
        mAvatarFullView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mAvatarFullView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void doOpenAnimation() {
        // translate mAvatarFullView so that it is centered on the mAvatarSmallView
        int mSmallCenterX = mStartX + mStartRadius / 2;
        int mSmallCenterY = mStartY + mStartRadius / 2;
        int[] coords = new int[2];
        mAvatarFullView.getLocationOnScreen(coords);
        int mFullCenterX = coords[0] + mAvatarFullView.getWidth() / 2;
        int mFullCenterY = coords[1] + mAvatarFullView.getHeight() / 2;
        mTranslateX = mSmallCenterX - mFullCenterX;
        mTranslateY = mSmallCenterY - mFullCenterY;
        mAvatarFullView.setTranslationX(mTranslateX);
        mAvatarFullView.setTranslationY(mTranslateY);
        // then reveal mAvatarFullView, starting from the center & same dimens as mAvatarSmallView
        mAvatarFullView.setVisibility(View.VISIBLE);
        Animator reveal = ViewAnimationUtils.createCircularReveal(
                mAvatarFullView,
                mAvatarFullView.getWidth() / 2,
                mAvatarFullView.getHeight() / 2,
                mAvatarSmallView.getWidth() / 2,
                (int) Math.hypot(mAvatarFullView.getWidth() / 2, mAvatarFullView.getHeight() / 2))
                                            .setDuration(mFabExpandDuration);
        // translate mAvatarFullView back into position along an arc
        GravityArcMotion arcMotion = new GravityArcMotion();
        arcMotion.setMinimumVerticalAngle(70f);
        Path motionPath = arcMotion.getPath(mTranslateX, mTranslateY, 0, 0);
        Animator position = ObjectAnimator.ofFloat(mAvatarFullView, View.TRANSLATION_X, View.TRANSLATION_Y, motionPath)
                                          .setDuration(mFabExpandDuration);

        mAvatarSmallView.setVisibility(View.GONE);
        // fade in mAvatarFullView
        Animator fadeInFull = ObjectAnimator.ofFloat(mAvatarFullView, View.ALPHA, 1f)
                                            .setDuration(mFabExpandDuration);
        // play 'em all together with the material interpolator
        AnimatorSet show = new AnimatorSet();
        show.setInterpolator(getFastOutSlowInInterpolator(FullAvatarActivity.this));
        show.playTogether(reveal, position, fadeInFull);
        show.start();
    }

    private void doCloseAnimation(Animator.AnimatorListener animationListener) {
        // fade out mAvatarFullView (rapidly)
        Animator fadeOutFull = ObjectAnimator.ofFloat(mAvatarFullView, View.ALPHA, 0f)
                                             .setDuration(mFabExpandDuration);

        // translate mAvatarFullView to center of mAvatarSmall position along an arc
        GravityArcMotion arcMotion = new GravityArcMotion();
        arcMotion.setMinimumVerticalAngle(70f);
        Path motionPath = arcMotion.getPath(0, 0, mTranslateX, mTranslateY);
        Animator position = ObjectAnimator.ofFloat(mAvatarFullView, View.TRANSLATION_X, View.TRANSLATION_Y, motionPath)
                                          .setDuration(mFabExpandDuration);
        // then hide mAvatarFullView, starting from the edge to same dimens as mAvatarSmallView
        Animator reveal = ViewAnimationUtils.createCircularReveal(
                mAvatarFullView,
                mAvatarFullView.getWidth() / 2,
                mAvatarFullView.getHeight() / 2,
                (int) Math.hypot(mAvatarFullView.getWidth() / 2, mAvatarFullView.getHeight() / 2),
                mAvatarSmallView.getWidth() / 2)
                                            .setDuration(mFabExpandDuration);
        // play 'em all together with the material interpolator
        AnimatorSet show = new AnimatorSet();
        show.setInterpolator(getFastOutSlowInInterpolator(FullAvatarActivity.this));
        show.playTogether(reveal, position, fadeOutFull);
        show.addListener(animationListener);
        show.start();
    }

    private int getStatusBarHeight() {
        if (mStatusBarHeight != 0) {
            return mStatusBarHeight;
        }
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            mStatusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return mStatusBarHeight;
    }

    @Override
    public void onBackPressed() {

        doCloseAnimation(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }

            @Override
            public void onAnimationEnd(Animator animation) {
                mAvatarFullView.setVisibility(View.GONE);
                FullAvatarActivity.super.onBackPressed();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mAvatarFullView.setVisibility(View.GONE);
                FullAvatarActivity.super.onBackPressed();
            }

            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
    }
}
