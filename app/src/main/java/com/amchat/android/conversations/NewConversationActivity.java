package com.amchat.android.conversations;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.amchat.android.AmchatApplication;
import com.amchat.android.R;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.firebase.FirebaseClient;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class NewConversationActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String              TAG          = "NewConversationActivity";
    private final        CompositeDisposable mDisposables = new CompositeDisposable();
    private List<SearchResult>     mSearchResults;
    private SearchResultsAdapter   mSearchResultsAdapter;
    private PublishSubject<String> mQueryEmitter;
    private FirebaseClient         mFirebaseClient;
    private DatabaseClient         mDatabaseClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_conversation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        RecyclerView usersRecyclerView = (RecyclerView) findViewById(R.id.users_recycler_view);
        usersRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        usersRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(usersRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        usersRecyclerView.addItemDecoration(dividerItemDecoration);
        mFirebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
        mDatabaseClient = new DatabaseClient(((AmchatApplication) getApplication()).getData());
        mSearchResults = new ArrayList<>();
        mSearchResultsAdapter = new SearchResultsAdapter(mSearchResults);
        mSearchResultsAdapter.setOnItemClickListener(new SearchResultsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                Intent chatIntent = new Intent(itemView.getContext(), ChatActivity.class);
                chatIntent.putExtra(ChatActivity.USER_NAME, mSearchResults.get(position).getName());
                chatIntent.putExtra(ChatActivity.USER_UID, mSearchResults.get(position).getId());
                itemView.getContext().startActivity(chatIntent);
                finish();
            }
        });
        usersRecyclerView.setAdapter(mSearchResultsAdapter);
        mQueryEmitter = PublishSubject.create();
        mDisposables.add(
                mQueryEmitter.debounce(200, TimeUnit.MILLISECONDS)
                             .distinctUntilChanged()
                             .switchMap(new Function<String, Observable<Pair<String, List<SearchResult>>>>() {
                                 @Override
                                 public Observable<Pair<String, List<SearchResult>>> apply(String query) throws Exception {
                                     return mFirebaseClient.searchUsernames(query).toObservable();
                                 }
                             })
                             .subscribeOn(Schedulers.io())
                             .observeOn(AndroidSchedulers.mainThread())
                             .subscribeWith(new DisposableObserver<Pair<String, List<SearchResult>>>() {
                                 @Override
                                 public void onNext(Pair<String, List<SearchResult>> searchResults) {
                                     //Log.d(TAG, "onNext: searchResults: " + searchResults);
                                     mSearchResults.clear();
                                     mSearchResults.addAll(searchResults.second);
                                     mSearchResultsAdapter.setQueryString(searchResults.first);
                                     mSearchResultsAdapter.notifyDataSetChanged();
                                 }

                                 @Override
                                 public void onError(Throwable e) {
                                     Log.e(TAG, "onError: ", e);
                                     mSearchResults.clear();
                                     mSearchResultsAdapter.notifyDataSetChanged();
                                 }

                                 @Override
                                 public void onComplete() {
                                     Log.d(TAG, "onComplete: ");
                                 }
                             }));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_new_conversation, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
        }
        if (searchView != null) {
            searchView.setIconified(false);
            searchView.setOnQueryTextListener(this);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //Log.d(TAG, "onQueryTextSubmit: " + query);
        mQueryEmitter.onNext(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //Log.d(TAG, "onQueryTextChange: " + newText);
        mQueryEmitter.onNext(newText);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDisposables.clear();
    }
}
