package com.amchat.android.conversations;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.amchat.android.AmchatApplication;
import com.amchat.android.R;
import com.amchat.android.database.DatabaseClient;
import com.amchat.android.firebase.FirebaseClient;
import com.amchat.android.login.LoginActivity;
import com.amchat.android.models.AmChatFirebaseInstanceIdEntity;
import com.amchat.android.models.Chat;
import com.amchat.android.models.Conversation;
import com.amchat.android.models.User;
import com.amchat.android.utils.Utilities;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.requery.reactivex.ReactiveResult;

public class ConversationsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String              TAG          = "ConversationsActivity";
    private static final String              NO_TOKEN     = "NO_DEVICE_TOKEN";
    private final        CompositeDisposable mDisposables = new CompositeDisposable();
    private List<Conversation>             mConversations;
    private ConversationsAdapter           mConversationsAdapter;
    private FirebaseAuth                   mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseClient                 mDatabaseClient;
    private FirebaseClient                 mFirebaseClient;
    private Chat                           mChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        //TransitionSet transitionSet = new TransitionSet();
        //transitionSet.addTransition(new ChangeBounds());
        //transitionSet.addTransition(new Fade());
        //getWindow().setSharedElementExitTransition(transitionSet);
        //getWindow().setSharedElementReenterTransition(transitionSet);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                }
                else {
                    // User is signed out. Show LoginActivity.
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    Intent conversationActivityIntent = new Intent(ConversationsActivity.this, LoginActivity.class);
                    ConversationsActivity.this.startActivity(conversationActivityIntent);
                    ConversationsActivity.this.finish();
                }
            }
        };
        //FirebaseClient firebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
        // If user is not authenticated, show the login activity
        if (mAuth.getCurrentUser() == null) {
            Intent conversationActivityIntent = new Intent(ConversationsActivity.this, LoginActivity.class);
            startActivity(conversationActivityIntent);
            finish();
            return;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newConversationIntent = new Intent(ConversationsActivity.this, NewConversationActivity.class);
                startActivity(newConversationIntent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mDatabaseClient = new DatabaseClient(((AmchatApplication) getApplication()).getData());
        mFirebaseClient = new FirebaseClient(FirebaseDatabase.getInstance());
        checkDeviceToken();

        // Setting username and email in drawer header
        mDisposables.add(
                mDatabaseClient.getCurrentUser()
                               .subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribeWith(new DisposableMaybeObserver<User>() {
                                   @Override
                                   public void onSuccess(User user) {
                                       ((TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_username)).setText(user.getName());
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       Log.e(TAG, "onError: ", e);
                                       ((TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_username)).setText(
                                               mAuth.getCurrentUser().getDisplayName());
                                   }

                                   @Override
                                   public void onComplete() { }
                               }));
        /*if (mAuth.getCurrentUser() != null) {
            ((TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_user_email)).setText(mAuth.getCurrentUser().getEmail());
        }*/
        //final View avatarImageView = navigationView.getHeaderView(0).findViewById(R.id.avatar_iv);
        final View avatarView = navigationView.getHeaderView(0).findViewById(R.id.avatar);
        avatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFullAvatarActivity(avatarView);
            }
        });

        RecyclerView conversationsRecyclerView = (RecyclerView) findViewById(R.id.conversations_recycler_view);
        conversationsRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        conversationsRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(conversationsRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        conversationsRecyclerView.addItemDecoration(dividerItemDecoration);
        mConversations = new ArrayList<>();
        mConversationsAdapter = new ConversationsAdapter(mConversations, mDatabaseClient);
        mConversationsAdapter.setOnAvatarClickListener(new ConversationsAdapter.OnAvatarClickListener() {
            @Override
            public void onAvatarClick(View view, int position) {
                showFullAvatarActivity(view);
            }
        });
        conversationsRecyclerView.setAdapter(mConversationsAdapter);
        fetchConversations();
    }

    private void showFullAvatarActivity(View avatarView) {
        Intent intent = new Intent(ConversationsActivity.this, FullAvatarActivity.class);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ConversationsActivity.this);
        Rect fullBounds = new Rect();
        avatarView.getDrawingRect(fullBounds);
        int radius = Math.max(fullBounds.width(), fullBounds.height());
        int[] coords = new int[2];
        avatarView.getLocationOnScreen(coords);
        intent.putExtra(FullAvatarActivity.EXTRA_START_X, coords[0]);
        intent.putExtra(FullAvatarActivity.EXTRA_START_Y, coords[1]);
        intent.putExtra(FullAvatarActivity.EXTRA_START_RADIUS, radius);
        startActivity(intent, options.toBundle());
    }

    private void checkDeviceToken() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "checkDeviceToken: token: " + token);
        mDisposables.add(
                mDatabaseClient.getInstanceId()
                               .toSingle(NO_TOKEN)
                               .filter(new Predicate<String>() {
                                   @Override
                                   public boolean test(String s) throws Exception {
                                       return s.equals(NO_TOKEN) || s.equals(token);
                                   }
                               })
                               .flatMap(new Function<String, Maybe<User>>() {
                                   @Override
                                   public Maybe<User> apply(String s) throws Exception {
                                       return mDatabaseClient.getCurrentUser();
                                   }
                               })
                               .flatMapSingle(new Function<User, Single<String>>() {
                                   @Override
                                   public Single<String> apply(User user) throws Exception {
                                       return mFirebaseClient.addDeviceTokenToUser(user, token);
                                   }
                               })
                               .doOnSuccess(new Consumer<String>() {
                                   @Override
                                   public void accept(String key) throws Exception {
                                       Log.d(TAG, "accept: Token added to firebase db: " + key);
                                   }
                               })
                               .flatMap(new Function<String, Single<AmChatFirebaseInstanceIdEntity>>() {
                                   @Override
                                   public Single<AmChatFirebaseInstanceIdEntity> apply(String s) throws Exception {
                                       return mDatabaseClient.updateInstanceId(token);
                                   }
                               })
                               .subscribeWith(new DisposableSingleObserver<AmChatFirebaseInstanceIdEntity>() {
                                   @Override
                                   public void onSuccess(AmChatFirebaseInstanceIdEntity instanceIdEntity) {
                                       Log.d(TAG, "onSuccess: Added token to local db");
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       Log.e(TAG, "onError: Error in saving token to local db", e);
                                   }
                               }));
    }

    private void listenToChats() {

        mDisposables.add(
                mDatabaseClient
                        .getChatsObserver()
                        .doOnNext(new Consumer<ReactiveResult<Chat>>() {
                            @Override
                            public void accept(ReactiveResult<Chat> chats) throws Exception {
                                //Log.d(TAG, "listenToChats: accept: chats: " + chats.toList());
                            }
                        })
                        .flatMapMaybe(new Function<ReactiveResult<Chat>, Maybe<Conversation>>() {
                            @Override
                            public Maybe<Conversation> apply(ReactiveResult<Chat> chats) throws Exception {
                                final List<Chat> chatList = chats.toList();
                                long conversationId = -1;
                                if (!chatList.isEmpty()) {
                                    // chats contains only 1 chat object
                                    Chat chat = chatList.get(0);
                                    mChat = chat;
                                    conversationId = chat.getConversationId();
                                }
                                return mDatabaseClient.getConversation(conversationId);
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Conversation>() {
                            @Override
                            public void accept(Conversation conversation) throws Exception {
                                int index = mConversations.indexOf(conversation);
                                if (index >= 0) {
                                    // If mConversations already contains this conversation
                                    if (mChat.getStatus() == Chat.STATUS_RECEIVED_UNREAD) {
                                        // Move this conversation to top
                                        mConversations.remove(index);
                                        mConversations.add(0, conversation);
                                        mConversationsAdapter.notifyItemMoved(index, 0);
                                        mConversationsAdapter.notifyItemChanged(0);
                                    }
                                    else {
                                        // just update it at its position.
                                        mConversations.set(index, conversation);
                                        mConversationsAdapter.notifyItemChanged(index);
                                    }
                                    return;
                                }
                                //Log.d(TAG, "accept: mChats does not contain: " + chat);
                                // New Conversation
                                mConversations.add(0, conversation);
                                mConversationsAdapter.notifyItemInserted(0);
                            }
                        }));
    }

    private void fetchConversations() {
        mDisposables.add(
                mDatabaseClient.getConversations()
                               .subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribeWith(new DisposableSingleObserver<List<Conversation>>() {
                                   @Override
                                   public void onSuccess(List<Conversation> conversations) {
                                       mConversations.addAll(conversations);
                                       mConversationsAdapter.notifyDataSetChanged();
                                       listenToChats();
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       Log.d(TAG, " onError : " + e.getMessage());
                                   }
                               }));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        }
        else if (id == R.id.nav_share) {

        }
        else if (id == R.id.nav_sign_out) {
            mAuth.signOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utilities.dismissUnreadNotification(ConversationsActivity.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // do not send event after activity has been destroyed
        mDisposables.clear();
    }
}
