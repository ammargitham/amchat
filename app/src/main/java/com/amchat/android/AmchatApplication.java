package com.amchat.android;

import android.app.Application;
import android.os.StrictMode;

import com.amchat.android.models.Models;

import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

/**
 * Created by Ammar Githam on 09/03/2017.
 */
public class AmchatApplication extends Application {

    private ReactiveEntityStore<Persistable> dataStore;
    private DatabaseSource                   source;

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.enableDefaults();
    }

    /**
     * @return {@link EntityDataStore} single instance for the application.
     */
    public ReactiveEntityStore<Persistable> getData() {
        if (dataStore == null) {
            if (BuildConfig.DEBUG) {
                //getApplicationContext().deleteDatabase("default");
            }
            // override onUpgrade to handle migrating to a new version
            source = new DatabaseSource(this, Models.DEFAULT, 1);
            //source.setLoggingEnabled(BuildConfig.DEBUG);
            if (BuildConfig.DEBUG) {
                // use this in development mode to drop and recreate the tables on every upgrade
                source.setTableCreationMode(TableCreationMode.DROP_CREATE);
            }
            Configuration configuration = source.getConfiguration();
            dataStore = ReactiveSupport.toReactiveStore(
                    new EntityDataStore<Persistable>(configuration));
        }
        return dataStore;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        source.close();
        dataStore.close();
    }
}
